﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.DAL.WMS
{
    public class DalT_WMS_ONDET :DalBase
   {
        public static IList<T_WMS_ONDET> GetIList(string SelectName,T_WMS_ONDET model)
        {
            return objMapper.GetMapper().QueryForList<T_WMS_ONDET>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_WMS_ONDET.DeleteT_WMS_ONDET", id);
        }

        public static decimal InsertData(T_WMS_ONDET model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void InsertData(List<T_WMS_ONDET> list)
        {
            objMapper.GetMapper().Insert("T_WMS_ONDET.InsertT_WMS_ONDET_List", list);
        }

        public static void DeleteDataAll(decimal id)
        {
            objMapper.GetMapper().Delete("T_WMS_ONDET.DeleteT_WMS_ONDET_ALL", id);
        }

        public static void UpdateData(T_WMS_ONDET model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

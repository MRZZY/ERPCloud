﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.DAL.WMS
{
    public class DalT_WMS_OUT :DalBase
   {
        public static IList<T_WMS_OUT> GetIList(string SelectName,T_WMS_OUT model)
        {
            return objMapper.GetMapper().QueryForList<T_WMS_OUT>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_WMS_OUT.DeleteT_WMS_OUT", id);
        }

        public static decimal InsertData(T_WMS_OUT model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_WMS_OUT model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.DAL.WMS
{
    public class DalT_WMS_IN :DalBase
   {
        public static IList<T_WMS_IN> GetIList(string SelectName,T_WMS_IN model)
        {
            return objMapper.GetMapper().QueryForList<T_WMS_IN>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_WMS_IN.DeleteT_WMS_IN", id);
        }

        public static decimal InsertData(T_WMS_IN model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_WMS_IN model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

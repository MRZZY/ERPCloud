﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.DAL.WMS
{
    public class DalT_WMS_INDET :DalBase
   {
        public static IList<T_WMS_INDET> GetIList(string SelectName,T_WMS_INDET model)
        {
            return objMapper.GetMapper().QueryForList<T_WMS_INDET>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_WMS_INDET.DeleteT_WMS_INDET", id);
        }

        public static void DeleteDataAll(decimal id)
        {
            objMapper.GetMapper().Delete("T_WMS_INDET.DeleteT_WMS_INDET_ALL", id);
        }

        public static decimal InsertData(T_WMS_INDET model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void InsertData(List<T_WMS_INDET> list)
        {
            objMapper.GetMapper().Insert("T_WMS_INDET.InsertT_WMS_INDET_List", list);
        }

        public static void UpdateData(T_WMS_INDET model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

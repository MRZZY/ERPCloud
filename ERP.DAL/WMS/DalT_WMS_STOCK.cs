﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.DAL.WMS
{
    public class DalT_WMS_STOCK :DalBase
   {
        public static IList<T_WMS_STOCK> GetIList(string SelectName,T_WMS_STOCK model)
        {
            return objMapper.GetMapper().QueryForList<T_WMS_STOCK>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_WMS_STOCK.DeleteT_WMS_STOCK", id);
        }

        public static decimal InsertData(T_WMS_STOCK model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void InsertData(List<T_WMS_STOCK> list)
        {
            objMapper.GetMapper().Insert("T_WMS_STOCK.InsertT_WMS_STOCK_List", list);
        }

        public static void UpStockData(List<T_WMS_STOCK> list)
        {
            objMapper.GetMapper().Update("T_WMS_STOCK.UpdateT_WMS_STOCK_UPSTCOK", list);
        }

        public static void UpdateData(T_WMS_STOCK model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;

namespace ERP.DAL.Base
{
    public class DalT_BASE_USER : DalBase
    {
        public static IList<T_BASE_USER> GetIList(string SelectName, T_BASE_USER model)
        {
            string sql = GetRuntimeSql(SelectName, model);
            return objMapper.GetMapper().QueryForList<T_BASE_USER>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_BASE_USER.DeleteT_BASE_USER", id);
        }

        public static decimal InsertData(T_BASE_USER model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_BASE_USER model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    }
}

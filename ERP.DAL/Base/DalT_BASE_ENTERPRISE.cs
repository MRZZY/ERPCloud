﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.DAL.Base
{
    public class DalT_BASE_ENTERPRISE :DalBase
   {
        public static IList<T_BASE_ENTERPRISE> GetIList(string SelectName,T_BASE_ENTERPRISE model)
        {
            return objMapper.GetMapper().QueryForList<T_BASE_ENTERPRISE>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_BASE_ENTERPRISE.DeleteT_BASE_ENTERPRISE", id);
        }

        public static decimal InsertData(T_BASE_ENTERPRISE model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_BASE_ENTERPRISE model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

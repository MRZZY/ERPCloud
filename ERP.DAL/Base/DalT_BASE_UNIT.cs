﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.DAL.Base
{
    public class DalT_BASE_UNIT :DalBase
   {
        public static IList<T_BASE_UNIT> GetIList(string SelectName,T_BASE_UNIT model)
        {
            return objMapper.GetMapper().QueryForList<T_BASE_UNIT>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_BASE_UNIT.DeleteT_BASE_UNIT", id);
        }

        public static decimal InsertData(T_BASE_UNIT model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_BASE_UNIT model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.DAL.Base
{
    public class DalT_BASE_VERIFICATION :DalBase
   {
        public static IList<T_BASE_VERIFICATION> GetIList(string SelectName,T_BASE_VERIFICATION model)
        {
            string sql = GetRuntimeSql(SelectName, model);
            return objMapper.GetMapper().QueryForList<T_BASE_VERIFICATION>(SelectName, model);
        }

        public static void DeleteData(int id)
        {
            objMapper.GetMapper().Delete("T_BASE_VERIFICATION.DeleteT_BASE_VERIFICATION", id);
        }

        public static decimal InsertData(T_BASE_VERIFICATION model)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(model.InsertName, model).ToString());
        }

        public static void UpdateData(T_BASE_VERIFICATION model)
        {
            objMapper.GetMapper().Update(model.UpdateName, model);
        }

    } 
}

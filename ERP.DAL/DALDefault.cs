﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.DAL
{
    /// <summary>
    /// 公用的数据处理
    /// </summary>
    public class DalDefault:DalBase
    {
        public static IList<T> GetIList<T>(string SelectName, T model) 
        {
            string sql = GetRuntimeSql(SelectName, model);
            return objMapper.GetMapper().QueryForList<T>(SelectName, model);
        }

        public static void DeleteData(int id,string deleteName)
        {
            objMapper.GetMapper().Delete(deleteName, id);
        }

        public static decimal InsertData<T>(T model,string insertName)
        {
            return decimal.Parse(objMapper.GetMapper().Insert(insertName, model).ToString());
        }

        public static void UpdateData<T>(T model,string UpdateName)
        {
            objMapper.GetMapper().Update(UpdateName, model);
        }

    }
}

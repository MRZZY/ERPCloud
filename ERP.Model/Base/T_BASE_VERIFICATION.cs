﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_VERIFICATION
    {
        public T_BASE_VERIFICATION(){ }

        /// <summary>
        /// 传值分钟数，验证指定分钟内是否有记录；
        /// 1、验证1分钟内是否有验证码，用于验证发送邮件的频率要间隔60秒
        /// 2、验证30分钟内是否有验证码，用于验证验证码有效期
        /// </summary>
        public decimal CHECKMIN
        {
            get;
            set;
        }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBV_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 接收人邮箱
        /// </summary>
        public string TBV_MAIL
        {
            set;
            get;
        }

        /// <summary>
        /// 验证码
        /// </summary>
        public string TBV_CODE
        {
            set;
            get;
        }

        /// <summary>
        /// 发送时间
        /// </summary>
        private DateTime _TBV_SEND_DATE = new DateTime(1900,1,1);
        public DateTime TBV_SEND_DATE
        {
            set{ _TBV_SEND_DATE = value; }
            get{ return _TBV_SEND_DATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_VERIFICATION";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_VERIFICATION_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_VERIFICATION.InsertT_BASE_VERIFICATION";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_VERIFICATION.DeleteT_BASE_VERIFICATION";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_VERIFICATION.UpdateT_BASE_VERIFICATION";}
        }

    } 
}

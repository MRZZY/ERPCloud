﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_SUPPLIER
    {
        public T_BASE_SUPPLIER(){ }

        /// <summary>
        /// 客户名称查询条件
        /// </summary>
        public string TBS_NAME_LIKE
        {
            get; set;
        }

        public string TBS_STATUS_LIKE
        {
            get;
            set;
        }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBS_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TSE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string TBS_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string TBS_FAX
        {
            set;
            get;
        }

        /// <summary>
        /// Email
        /// </summary>
        public string TBS_EMAIL
        {
            set;
            get;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string TBS_LINKMAN
        {
            set;
            get;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string TBS_TEL
        {
            set;
            get;
        }

        /// <summary>
        /// 联系手机
        /// </summary>
        public string TBS_MOBILE
        {
            set;
            get;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string TBS_ADDER
        {
            set;
            get;
        }

        /// <summary>
        /// 支付宝账号
        /// </summary>
        public string TBS_ZFB
        {
            set;
            get;
        }

        /// <summary>
        /// 微信账号
        /// </summary>
        public string TBS_WCHAR
        {
            set;
            get;
        }

        /// <summary>
        /// 开户名称
        /// </summary>
        public string TBS_BANKNAME
        {
            set;
            get;
        }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string TBS_BANK
        {
            set;
            get;
        }

        /// <summary>
        /// 银行账号
        /// </summary>
        public string TBS_BANKCODE
        {
            set;
            get;
        }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string TBS_FAXHEARD
        {
            set;
            get;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string TBS_STATUS
        {
            set;
            get;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string TBS_REMARK
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_SUPPLIER";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_SUPPLIER_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_SUPPLIER.InsertT_BASE_SUPPLIER";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_SUPPLIER.DeleteT_BASE_SUPPLIER";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_SUPPLIER.UpdateT_BASE_SUPPLIER";}
        }

    } 
}

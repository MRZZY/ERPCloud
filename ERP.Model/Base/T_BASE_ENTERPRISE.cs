﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_ENTERPRISE
    {
        public T_BASE_ENTERPRISE(){ }


        #region 查询

        /// <summary>
        /// 查询账号所有企业，含创建和加入
        /// </summary>
        public decimal TBA_DID_ENT
        {
            get;set;
        }

        #endregion

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业对应用户
        /// </summary>
        public string All_USER
        {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string TBE_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string TBE_REMARK
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        /// <summary>
        /// 账号主键
        /// </summary>
        public decimal TBA_DID
        {
            set;
            get;
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_ENTERPRISE";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_ENTERPRISE_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_ENTERPRISE.InsertT_BASE_ENTERPRISE";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_ENTERPRISE.DeleteT_BASE_ENTERPRISE";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_ENTERPRISE.UpdateT_BASE_ENTERPRISE";}
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_USER
    {
        public T_BASE_USER(){ }
        /// <summary>
        /// 限制查询企业创建人
        /// </summary>
        public decimal NOT_TBA_DID
        {
            get;set;
        }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBU_DID
        {
            set;
            get;
        }


        /// <summary>
        /// 头像路径
        /// </summary>
        public string TBA_HPHOTO
        {
            get;
            set;
        }

        /// <summary>
        /// 电话
        /// </summary>
        public decimal TBA_IPHONE
        {
            get;
            set;
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string TBA_EMAIL
        {
            get;
            set;
        }

        /// <summary>
        /// 企业ID
        /// </summary>
        public decimal TBE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string TBA_NAME
        {
            get;
            set;
        }

        /// <summary>
        /// 企业对应用户
        /// </summary>
        public string All_USER
        {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string TBE_NAME
        {
            get;
            set;
        }

        /// <summary>
        /// 企业描述
        /// </summary>
        public string TBE_REMARK
        {
            get;set;
        }

        /// <summary>
        /// 查询账号所有企业，含创建和加入
        /// </summary>
        public decimal TBA_DID_ENT
        {
            get; set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string TBU_STATE
        {
            set;
            get;
        }

        /// <summary>
        /// 账号主键
        /// </summary>
        public decimal TBA_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 单据权限
        /// </summary>
        public string TBU_BILL
        {
            set;
            get;
        }

        /// <summary>
        /// 仓库权限
        /// </summary>
        public string TBU_WARES
        {
            set;
            get;
        }

        /// <summary>
        /// 角色权限
        /// </summary>
        public string TBU_ROLES
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public decimal CREATEID
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CRREATETIME = new DateTime(1900,1,1);
        public DateTime CRREATETIME
        {
            set{ _CRREATETIME = value; }
            get{ return _CRREATETIME; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public decimal UPDATEID
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATETIME = new DateTime(1900,1,1);
        public DateTime UPDATETIME
        {
            set{ _UPDATETIME = value; }
            get{ return _UPDATETIME; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_USER";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_USER_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_USER.InsertT_BASE_USER";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_USER.DeleteT_BASE_USER";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_USER.UpdateT_BASE_USER";}
        }

    } 
}

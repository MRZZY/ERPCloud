﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_ACCOUNT
    {
        public T_BASE_ACCOUNT(){ }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBA_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 姓名
        /// </summary>
        public string TBA_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 手机号码
        /// </summary>
        public decimal TBA_IPHONE
        {
            set;
            get;
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string TBA_EMAIL
        {
            set;
            get;
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string TBA_PASSWORD
        {
            set;
            get;
        }

        /// <summary>
        /// 头像路径
        /// </summary>
        public string TBA_HPHOTO
        {
            set;
            get;
        }

        /// <summary>
        /// 更新ID
        /// </summary>
        public decimal UPDATEID
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATETIME = new DateTime(1900,1,1);
        public DateTime UPDATETIME
        {
            set{ _UPDATETIME = value; }
            get{ return _UPDATETIME; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_ACCOUNT";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_ACCOUNT_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_ACCOUNT.InsertT_BASE_ACCOUNT";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_ACCOUNT.DeleteT_BASE_ACCOUNT";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_ACCOUNT.UpdateT_BASE_ACCOUNT";}
        }

    } 
}

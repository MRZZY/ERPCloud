﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_GOODS
    {
        public T_BASE_GOODS(){ }

        /// <summary>
        /// 合并显示字段
        /// </summary>
        public string MERGE_TEXT { get; set; }

        /// <summary>
        /// 合并显示字段，带单价
        /// </summary>
        public string MERGE_P_TEXT { get; set; }

        /// <summary>
        /// 状态查询条件
        /// </summary>
        public string TBG_STATUS_LIKE { get; set; }

        /// <summary>
        /// 名称/规格模糊查询
        /// </summary>
        public string NAME_SPEC { get; set; }

        /// <summary>
        /// 商品名称模糊查询
        /// </summary>
        public string TBG_NAME_LIKE
        {
            get;
            set;
        }

        /// <summary>
        /// 商品规格模糊查询
        /// </summary>
        public string TBG_SPEC_LIKE
        {
            get;
            set;
        }

        /// <summary>
        /// 状态名称
        /// </summary>
        public string TBG_STATUS_TEXT
        {
            get;set;
        }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string TBC_NAME
        { get; set; }


        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBG_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string TBG_NUIT
        {
            set;
            get;
        }

        /// <summary>
        /// 市场价格
        /// </summary>
        public decimal TBG_PRICE
        {
            get;set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string TBG_STATUS
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TSE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBC_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string TBG_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string TBG_SPEC
        {
            set;
            get;
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_GOODS";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_GOODS_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_GOODS.InsertT_BASE_GOODS";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_GOODS.DeleteT_BASE_GOODS";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_GOODS.UpdateT_BASE_GOODS";}
        }

    } 
}

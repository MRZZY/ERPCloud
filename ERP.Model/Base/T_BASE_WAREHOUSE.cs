﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_WAREHOUSE
    {
        public T_BASE_WAREHOUSE(){ }

        /// <summary>
        /// 状态名称
        /// </summary>
        public string TBW_STATUS_TEXT
        { get; set; }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TBW_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TSE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string TBW_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 仓库地址
        /// </summary>
        public string TBW_ADD
        {
            set;
            get;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string TBW_STATUS
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_WAREHOUSE";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_WAREHOUSE_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_WAREHOUSE.InsertT_BASE_WAREHOUSE";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_WAREHOUSE.DeleteT_BASE_WAREHOUSE";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_WAREHOUSE.UpdateT_BASE_WAREHOUSE";}
        }

    } 
}

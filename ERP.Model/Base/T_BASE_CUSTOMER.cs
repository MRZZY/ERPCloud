﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base; 

namespace ERP.Model.Base
{
    public class T_BASE_CUSTOMER
    {
        public T_BASE_CUSTOMER(){ }

        /// <summary>
        /// 客户名称查询条件
        /// </summary>
        public string TSC_NAME_LIKE
        {
            get;set;
        }

        public string TSC_STATUS_LIKE
        {
            get;
            set;
        }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TSC_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TSE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string TSC_NAME
        {
            set;
            get;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string TSC_FAX
        {
            set;
            get;
        }

        /// <summary>
        /// Email
        /// </summary>
        public string TSC_EMAIL
        {
            set;
            get;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string TSC_LINKMAN
        {
            set;
            get;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string TSC_LINKTEL
        {
            set;
            get;
        }

        /// <summary>
        /// 联系手机
        /// </summary>
        public string TSC_LINKMOBILE
        {
            set;
            get;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string TSC_ADDER
        {
            set;
            get;
        }

        /// <summary>
        /// 支付宝账号
        /// </summary>
        public string TSC_ZFB
        {
            set;
            get;
        }

        /// <summary>
        /// 微信账号
        /// </summary>
        public string TSC_WCHAR
        {
            set;
            get;
        }

        /// <summary>
        /// 开户名称
        /// </summary>
        public string TSC_BANKNAME
        {
            set;
            get;
        }

        /// <summary>
        /// 开户银行
        /// </summary>
        public string TSC_BANK
        {
            set;
            get;
        }

        /// <summary>
        /// 银行账号
        /// </summary>
        public string TSC_BANKCODE
        {
            set;
            get;
        }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string TSC_FAXHEARD
        {
            set;
            get;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string TSC_STATUS
        {
            set;
            get;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string TSC_REMARL
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_BASE_CUSTOMER";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_BASE_CUSTOMER_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_BASE_CUSTOMER.InsertT_BASE_CUSTOMER";}
        }

        public string DeleteName
        {
            get{ return "T_BASE_CUSTOMER.DeleteT_BASE_CUSTOMER";}
        }

        public string UpdateName
        {
            get{ return "T_BASE_CUSTOMER.UpdateT_BASE_CUSTOMER";}
        }

    } 
}

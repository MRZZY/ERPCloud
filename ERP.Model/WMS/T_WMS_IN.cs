﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.Model.WMS
{
    public class T_WMS_IN
    {
        public T_WMS_IN(){ }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TWI_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string TBS_NAME {
            get;
            set;
        }

        /// <summary>
        /// 制单人名称
        /// </summary>
        public string CREATER_NAME { get; set; }

        /// <summary>
        /// 日期格式化
        /// </summary>
        public string TWI_DATE_F { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string TBW_NAME { get; set; }

        /// <summary>
        /// 供应商主键
        /// </summary>
        public decimal TBS_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TBE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 仓库主键
        /// </summary>
        public decimal TBW_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 入库日期
        /// </summary>
        private DateTime _TWI_DATE = new DateTime(1900,1,1);
        public DateTime TWI_DATE
        {
            set{ _TWI_DATE = value; }
            get{ return _TWI_DATE; }
        }

        private DateTime _TWI_DATE_BEGIN = new DateTime(1900, 1, 1);
        public DateTime TWI_DATE_BEGIN
        {
            set { _TWI_DATE_BEGIN = value; }
            get { return _TWI_DATE_BEGIN; }
        }

        private DateTime _TWI_DATE_END = new DateTime(1900, 1, 1);
        public DateTime TWI_DATE_END
        {
            set { _TWI_DATE_END = value; }
            get { return _TWI_DATE_END; }
        }


        /// <summary>
        /// 入库单号
        /// </summary>
        public string TWI_DOC
        {
            set;
            get;
        }

        /// <summary>
        /// 经办人
        /// </summary>
        public string TWI_OPERATOR
        {
            set;
            get;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string TWI_REMARKS
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_WMS_IN";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_WMS_IN_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_WMS_IN.InsertT_WMS_IN";}
        }

        public string DeleteName
        {
            get{ return "T_WMS_IN.DeleteT_WMS_IN";}
        }

        public string UpdateName
        {
            get{ return "T_WMS_IN.UpdateT_WMS_IN";}
        }

        public List<T_WMS_INDET> detList = new List<T_WMS_INDET>();

    } 
}

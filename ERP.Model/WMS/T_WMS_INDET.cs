﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;

namespace ERP.Model.WMS
{
    public class T_WMS_INDET
    {
        public T_WMS_INDET() { }

        /// <summary>
        /// 仓库ID
        /// </summary>
        public decimal TBW_DID { get; set; }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        { get; set; }
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        { get; set; }
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        { get; set; }

        public string TBG_NAME { get; set; }
        public string TBG_SPEC { get; set; }
        public string TBG_NUIT { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public decimal TWID_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 入库主键
        /// </summary>
        public decimal TWI_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 供应商信息主键
        /// </summary>
        public decimal TBG_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal TWID_NUM
        {
            set;
            get;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public decimal TWID_PRICE
        {
            set;
            get;
        }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal TWID_MONEY
        {
            set;
            get;
        }

        public string SelectName
        {
            get { return "SelectT_WMS_INDET"; }
        }

        public string SelectNamePage
        {
            get { return "SelectT_WMS_INDET_PAGE"; }
        }

        public string InsertName
        {
            get { return "T_WMS_INDET.InsertT_WMS_INDET"; }
        }

        public string DeleteName
        {
            get { return "T_WMS_INDET.DeleteT_WMS_INDET"; }
        }

        public string UpdateName
        {
            get { return "T_WMS_INDET.UpdateT_WMS_INDET"; }
        }

    }
}

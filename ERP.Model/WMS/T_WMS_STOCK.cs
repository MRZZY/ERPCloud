﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.Model.WMS
{
    public class T_WMS_STOCK
    {
        public T_WMS_STOCK(){ }

        /// <summary>
        /// 不等于0库存
        /// </summary>
        public decimal NOT_ZERO { get; set; }

        public string NAME_SPEC { get; set; }
        public string TBC_NAME { get; set; }
        public string TBW_NAME { get; set; }
        public string TBG_NAME { get; set; }
        public string TBG_SPEC { get; set; }
        public string TBG_NUIT { get; set; }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TWS_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TBE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 仓库主键
        /// </summary>
        public decimal TBW_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 供应商信息主键
        /// </summary>
        public decimal TBG_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal TWID_NUM
        {
            set;
            get;
        }

        public string SelectName
        {
            get{ return "SelectT_WMS_STOCK";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_WMS_STOCK_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_WMS_STOCK.InsertT_WMS_STOCK";}
        }

        public string DeleteName
        {
            get{ return "T_WMS_STOCK.DeleteT_WMS_STOCK";}
        }

        public string UpdateName
        {
            get{ return "T_WMS_STOCK.UpdateT_WMS_STOCK";}
        }

    } 
}

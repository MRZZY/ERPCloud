﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS; 

namespace ERP.Model.WMS
{
    public class T_WMS_OUT
    {
        public T_WMS_OUT(){ }

        public string TSC_LINKMAN { get; set; }

        public string TSC_LINKMOBILE { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public decimal SUM_MONEY { get; set; }

        /// <summary>
        /// 日期格式化 年月日
        /// </summary>
        public string TWO_DATE_T { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DISCOUNT { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string TBW_NAME { get; set; }

        /// <summary>
        /// 制单人名称
        /// </summary>
        public string CREATER_NAME { get; set; }

        /// <summary>
        /// 日期格式化
        /// </summary>
        public string TWO_DATE_F { get; set; }
        
        private DateTime _TWO_DATE_BEGIN = new DateTime(1900, 1, 1);
        public DateTime TWO_DATE_BEGIN
        {
            set { _TWO_DATE_BEGIN = value; }
            get { return _TWO_DATE_BEGIN; }
        }

        private DateTime _TWO_DATE_END = new DateTime(1900, 1, 1);
        public DateTime TWO_DATE_END
        {
            set { _TWO_DATE_END = value; }
            get { return _TWO_DATE_END; }
        }

        /// <summary>
        /// 总数
        /// <summary>
        public int PAGECOUNT
        {get;set;}
        /// <summary>
        /// 一页显示个数
        /// <summary>
        public int BEGSIZE
        {get;set;}
        /// <summary>
        /// 当前页数
        /// <summary>
        public int PAGEINDEX
        {get;set;}
        /// <summary>
        /// 主键
        /// </summary>
        public decimal TWO_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 应付金额
        /// </summary>
        public decimal TWO_MONEY
        {
            set;
            get;
        }

        public string TWO_AGR_T { get; set; }

        /// <summary>
        /// 是否协议
        /// </summary>
        public bool TWO_AGR
        {
            set;
            get;
        }

        /// <summary>
        /// 客户ID
        /// </summary>
        public decimal TSC_DID
        { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string TSC_NAME
        { get; set; }

        /// <summary>
        /// 企业信息主键
        /// </summary>
        public decimal TBE_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 仓库主键
        /// </summary>
        public decimal TBW_DID
        {
            set;
            get;
        }

        /// <summary>
        /// 出库日期
        /// </summary>
        private DateTime _TWO_DATE = new DateTime(1900,1,1);
        public DateTime TWO_DATE
        {
            set{ _TWO_DATE = value; }
            get{ return _TWO_DATE; }
        }

        /// <summary>
        /// 出库单号
        /// </summary>
        public string TWO_DOC
        {
            set;
            get;
        }

        /// <summary>
        /// 经办人
        /// </summary>
        public string TWO_OPERATOR
        {
            set;
            get;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string TWO_REMARKS
        {
            set;
            get;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATER
        {
            set;
            get;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime _CREATEDATE = new DateTime(1900,1,1);
        public DateTime CREATEDATE
        {
            set{ _CREATEDATE = value; }
            get{ return _CREATEDATE; }
        }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UPDATER
        {
            set;
            get;
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        private DateTime _UPDATEDATE = new DateTime(1900,1,1);
        public DateTime UPDATEDATE
        {
            set{ _UPDATEDATE = value; }
            get{ return _UPDATEDATE; }
        }

        public string SelectName
        {
            get{ return "SelectT_WMS_OUT";}
        }

        public string SelectNamePage
        {
            get{ return "SelectT_WMS_OUT_PAGE";}
        }

        public string InsertName
        {
            get{ return "T_WMS_OUT.InsertT_WMS_OUT";}
        }

        public string DeleteName
        {
            get{ return "T_WMS_OUT.DeleteT_WMS_OUT";}
        }

        public string UpdateName
        {
            get{ return "T_WMS_OUT.UpdateT_WMS_OUT";}
        }

        public List<T_WMS_ONDET> detList = new List<T_WMS_ONDET>();

    } 
}

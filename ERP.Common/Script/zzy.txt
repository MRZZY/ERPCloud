﻿
/*==============================================================*/
/* Table: T_WMS_ONDET                                           */
/*==============================================================*/
create table T_WMS_ONDET 
(
   TWO_DID              NUMBER,
   TWOD_DID             NUMBER               not null,
   TBG_DID              NUMBER               not null,
   TWID_NUM             NUMBER,
   constraint PK_T_WMS_ONDET primary key (TWOD_DID)
);

comment on table T_WMS_ONDET is
'出库明细表';

comment on column T_WMS_ONDET.TWO_DID is
'出库主键';

comment on column T_WMS_ONDET.TWOD_DID is
'明细主键';

comment on column T_WMS_ONDET.TBG_DID is
'供应商信息主键';

comment on column T_WMS_ONDET.TWID_NUM is
'数量';



-- Create sequence 
create sequence SEQ_T_WMS_ONDET
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: T_WMS_OUT                                             */
/*==============================================================*/
create table T_WMS_OUT 
(
   TBE_DID              NUMBER,
   TSC_DID              NUMBER,
   TBW_DID              NUMBER,
   TWO_DID              NUMBER               not null,
   TWO_DATE             DATE,
   TWO_DOC              nvarchar2(50),
   TWO_OPERATOR         nvarchar2(50),
   TWO_REMARKS          nvarchar2(400),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_WMS_OUT primary key (TWO_DID)
);

comment on table T_WMS_OUT is
'出库主表';

comment on column T_WMS_OUT.TBE_DID is
'企业信息主键';

comment on column T_WMS_OUT.TBW_DID is
'仓库主键';

comment on column T_WMS_OUT.TWO_DID is
'出库主键';

comment on column T_WMS_OUT.TWO_DATE is
'出库日期';

comment on column T_WMS_OUT.TWO_DOC is
'出库单号';

comment on column T_WMS_OUT.TWO_OPERATOR is
'经办人';

comment on column T_WMS_OUT.TWO_REMARKS is
'备注';

comment on column T_WMS_OUT.CREATER is
'创建人';

comment on column T_WMS_OUT.CREATEDATE is
'创建时间';

comment on column T_WMS_OUT.UPDATER is
'更新人';

comment on column T_WMS_OUT.UPDATEDATE is
'更新时间';



-- Create sequence 
create sequence SEQ_T_WMS_OUT
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: T_WMS_INDET                                           */
/*==============================================================*/
create table T_WMS_INDET 
(
   TWI_DID              NUMBER,
   TWID_DID             NUMBER               not null,
   TBG_DID              NUMBER               not null,
   TWID_NUM             NUMBER,
   constraint PK_T_WMS_INDET primary key (TWID_DID)
);

comment on table T_WMS_INDET is
'入库明细表';

comment on column T_WMS_INDET.TWI_DID is
'入库主键';

comment on column T_WMS_INDET.TWID_DID is
'明细主键';

comment on column T_WMS_INDET.TBG_DID is
'供应商信息主键';

comment on column T_WMS_INDET.TWID_NUM is
'数量';


-- Create sequence 
create sequence SEQ_T_WMS_INDET
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: T_WMS_IN                                              */
/*==============================================================*/
create table T_WMS_IN 
(
   TBE_DID              NUMBER,
   TBS_DID              NUMBER,
   TBW_DID              NUMBER               not null,
   TWI_DID              NUMBER               not null,
   TWI_DATE             DATE,
   TWI_DOC              nvarchar2(50),
   TWI_OPERATOR         nvarchar2(50),
   TWI_REMARKS          nvarchar2(400),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_WMS_IN primary key (TWI_DID)
);

comment on table T_WMS_IN is
'入库主表';

comment on column T_WMS_IN.TBE_DID is
'企业信息主键';

comment on column T_WMS_IN.TBW_DID is
'仓库主键';

comment on column T_WMS_IN.TWI_DID is
'入库主键';

comment on column T_WMS_IN.TWI_DATE is
'入库日期';

comment on column T_WMS_IN.TWI_DOC is
'入库单号';

comment on column T_WMS_IN.TWI_OPERATOR is
'经办人';

comment on column T_WMS_IN.TWI_REMARKS is
'备注';

comment on column T_WMS_IN.CREATER is
'创建人';

comment on column T_WMS_IN.CREATEDATE is
'创建时间';

comment on column T_WMS_IN.UPDATER is
'更新人';

comment on column T_WMS_IN.UPDATEDATE is
'更新时间';


-- Create sequence 
create sequence SEQ_T_WMS_IN
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: T_WMS_STOCK                                           */
/*==============================================================*/
create table T_WMS_STOCK 
(
   TBE_DID              NUMBER,
   TWS_DID              NUMBER               not null,
   TBW_DID              NUMBER,
   TBG_DID              NUMBER,
   TWID_NUM             NUMBER,
   constraint PK_T_WMS_STOCK primary key (TWS_DID)
);

comment on table T_WMS_STOCK is
'库存表';

comment on column T_WMS_STOCK.TBE_DID is
'企业信息主键';

comment on column T_WMS_STOCK.TWS_DID is
'库存主键';

comment on column T_WMS_STOCK.TBW_DID is
'仓库主键';

comment on column T_WMS_STOCK.TBG_DID is
'供应商信息主键';

comment on column T_WMS_STOCK.TWID_NUM is
'库存数量';

-- Create sequence 
create sequence SEQ_T_WMS_STOCK
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: T_BASE_GOODS                                          */
/*==============================================================*/
create table T_BASE_GOODS 
(
   TSE_DID              NUMBER,
   TBC_DID              NUMBER,
   TBG_DID              NUMBER               not null,
   TBG_NAME             nvarchar2(50),
   TBG_SPEC             nvarchar2(100),
   TBG_NUIT             nvarchar2(200),
   TBG_STATUS           nvarchar2(50),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_GOODS primary key (TBG_DID)
);

comment on table T_BASE_GOODS is
'商品信息';

comment on column T_BASE_GOODS.TSE_DID is
'企业信息主键';

comment on column T_BASE_GOODS.TBC_DID is
'供应商信息主键';

comment on column T_BASE_GOODS.TBG_DID is
'供应商信息主键';

comment on column T_BASE_GOODS.TBG_NAME is
'供应商名称';

comment on column T_BASE_GOODS.TBG_SPEC is
'规格型号';

comment on column T_BASE_GOODS.TBG_NUIT is
'计量单位';

comment on column T_BASE_GOODS.TBG_STATUS is
'状态';

comment on column T_BASE_GOODS.CREATER is
'创建人';

comment on column T_BASE_GOODS.CREATEDATE is
'创建时间';

comment on column T_BASE_GOODS.UPDATER is
'更新人';

comment on column T_BASE_GOODS.UPDATEDATE is
'更新时间';


-- Create sequence 
create sequence SEQ_T_BASE_GOODS
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;
/*==============================================================*/
/* Table: T_BASE_CATEGORIES                                     */
/*==============================================================*/
create table T_BASE_CATEGORIES 
(
   TSE_DID              NUMBER,
   TBC_DID              NUMBER               not null,
   TBC_PARENT           NUMBER,
   TBC_NAME             nvarchar2(50),
   TBC_CODE             nvarchar2(50),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_CATEGORIES primary key (TBC_DID)
);

comment on table T_BASE_CATEGORIES is
'商品分类';

comment on column T_BASE_CATEGORIES.TBC_CODE is
'分类编号';

comment on column T_BASE_CATEGORIES.TSE_DID is
'企业信息主键';

comment on column T_BASE_CATEGORIES.TBC_DID is
'供应商信息主键';

comment on column T_BASE_CATEGORIES.TBC_PARENT is
'上级ID';

comment on column T_BASE_CATEGORIES.TBC_NAME is
'供应商名称';

comment on column T_BASE_CATEGORIES.CREATER is
'创建人';

comment on column T_BASE_CATEGORIES.CREATEDATE is
'创建时间';

comment on column T_BASE_CATEGORIES.UPDATER is
'更新人';

comment on column T_BASE_CATEGORIES.UPDATEDATE is
'更新时间';


-- Create sequence 
create sequence SEQ_T_BASE_CATEGORIES
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;

/*==============================================================*/
/* Table: T_BASE_WAREHOUSE                                      */
/*==============================================================*/
create table T_BASE_WAREHOUSE 
(
   TSE_DID              NUMBER,
   TBW_DID              NUMBER               not null,
   TBW_NAME             nvarchar2(50),
   TBW_ADD              nvarchar2(200),
   TBW_STATUS           nvarchar2(50),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_WAREHOUSE primary key (TBW_DID)
);

comment on table T_BASE_WAREHOUSE is
'仓库信息';

comment on column T_BASE_WAREHOUSE.TSE_DID is
'企业信息主键';

comment on column T_BASE_WAREHOUSE.TBW_DID is
'供应商信息主键';

comment on column T_BASE_WAREHOUSE.TBW_NAME is
'供应商名称';

comment on column T_BASE_WAREHOUSE.TBW_ADD is
'仓库地址';

comment on column T_BASE_WAREHOUSE.TBW_STATUS is
'状态';

comment on column T_BASE_WAREHOUSE.CREATER is
'创建人';

comment on column T_BASE_WAREHOUSE.CREATEDATE is
'创建时间';

comment on column T_BASE_WAREHOUSE.UPDATER is
'更新人';

comment on column T_BASE_WAREHOUSE.UPDATEDATE is
'更新时间';


/*==============================================================*/
/* Table: T_BASE_UNIT                                           */
/*==============================================================*/
create table T_BASE_UNIT 
(
   TSE_DID              NUMBER,
   TBU_DID              NUMBER               not null,
   TBU_NAME             nvarchar2(50),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_UNIT primary key (TBU_DID)
);

comment on table T_BASE_UNIT is
'计量单位';

comment on column T_BASE_UNIT.TSE_DID is
'企业信息主键';

comment on column T_BASE_UNIT.TBU_DID is
'供应商信息主键';

comment on column T_BASE_UNIT.TBU_NAME is
'供应商名称';

comment on column T_BASE_UNIT.CREATER is
'创建人';

comment on column T_BASE_UNIT.CREATEDATE is
'创建时间';

comment on column T_BASE_UNIT.UPDATER is
'更新人';

comment on column T_BASE_UNIT.UPDATEDATE is
'更新时间';

-- Create sequence 
create sequence SEQ_T_BASE_UNIT
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;

-- Create sequence 
create sequence SEQ_T_BASE_WAREHOUSE
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;




/*==============================================================*/
/* Table: 验证码表                                   */
/*==============================================================*/
create table T_BASE_VERIFICATION 
(
   TBV_DID              NUMBER               not null,
   TBV_MAIL             VARCHAR(50),
   TBV_CODE             VARCHAR(50),
   TBV_SEND_DATE        DATE,
   constraint PK_T_BASE_VERIFICATION primary key (TBV_DID)
);

comment on column T_BASE_VERIFICATION.TBV_DID is
'验证码主键';

comment on column T_BASE_VERIFICATION.TBV_MAIL is
'接收人邮箱';

comment on column T_BASE_VERIFICATION.TBV_CODE is
'验证码';

comment on column T_BASE_VERIFICATION.TBV_SEND_DATE is
'发送时间';


/*==============================================================*/
/* Table: 用户表                                           */
/*==============================================================*/
create table T_BASE_USER 
(
   TBU_DID              NUMBER               not null,
   TBA_DID              NUMBER,
   TBE_DID				NUMBER,
   TBU_STATE			VARCHAR2(2),
   TBU_NAME             VARCHAR2(15),
   TBU_IPHONE           NUMBER,
   TBU_EMAIL            VARCHAR2(50),
   TBU_BILL             VARCHAR2(2),
   TBU_WARES            VARCHAR2(70),
   TBU_ROLES            VARCHAR2(70),
   CREATEID             INTEGER,
   CRREATETIME          DATE,
   UPDATEID             INTEGER,
   UPDATETIME           DATE,
   constraint PK_T_BASE_USER primary key (TBU_DID)
);
comment on column T_BASE_USER.TBE_DID is
'企业ID';

comment on column T_BASE_USER.TBA_DID is
'账号主键';

comment on column T_BASE_USER.TBU_STATE is
'状态 0：正常 1：停用';

comment on column T_BASE_USER.TBU_DID is
'用户ID';

comment on column T_BASE_USER.TBU_NAME is
'姓名';

comment on column T_BASE_USER.TBU_IPHONE is
'手机号码';

comment on column T_BASE_USER.TBU_EMAIL is
'邮箱';

comment on column T_BASE_USER.TBU_BILL is
'单据权限 - 只能看自己';

comment on column T_BASE_USER.TBU_WARES is
'仓库权限';

comment on column T_BASE_USER.TBU_ROLES is
'角色权限';

comment on column T_BASE_USER.CREATEID is
'创建者ID';

comment on column T_BASE_USER.CRREATETIME is
'创建时间';

comment on column T_BASE_USER.UPDATEID is
'更新ID';

comment on column T_BASE_USER.UPDATETIME is
'更新时间';

-- Create sequence 
create sequence SEQ_T_BASE_USER
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


/*==============================================================*/
/* Table: 账号表                                        */
/*==============================================================*/
create table T_BASE_ACCOUNT 
(
   TBA_DID              NUMBER               not null,
   TBA_NAME             VARCHAR2(15),
   TBA_IPHONE           NUMBER,
   TBA_EMAIL            VARCHAR2(50),
   TBA_PASSWORD         VARCHAR2(32),
   TBA_HPHOTO           VARCHAR2(80),
   UPDATEID             INTEGER,
   UPDATETIME           DATE,
   constraint PK_T_BASE_ACCOUNT primary key (TBA_DID)
);

comment on column T_BASE_ACCOUNT.TBA_DID is
'账号ID';

comment on column T_BASE_ACCOUNT.TBA_NAME is
'姓名';

comment on column T_BASE_ACCOUNT.TBA_IPHONE is
'手机号码';

comment on column T_BASE_ACCOUNT.TBA_EMAIL is
'邮箱';

comment on column T_BASE_ACCOUNT.TBA_PASSWORD is
'密码';

comment on column T_BASE_ACCOUNT.TBA_HPHOTO is
'头像路径';

comment on column T_BASE_ACCOUNT.UPDATEID is
'更新ID';

comment on column T_BASE_ACCOUNT.UPDATETIME is
'更新时间';


/*==============================================================*/
/* Table: T_BASE_ENTERPRISE                                     */
/*==============================================================*/
create table T_BASE_ENTERPRISE 
(
   TBE_DID              NUMBER              not null,
   TBA_DID              NUMBER,
   TBE_NAME             VARCHAR2(50),
   TBE_REMARK           VARCHAR2(500),
   CREATER              VARCHAR2(50),
   CREATEDATE           DATE,
   UPDATER              VARCHAR2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_ENTERPRISE primary key (TBE_DID)
);

comment on table T_BASE_ENTERPRISE is
'企业信息';

comment on column T_BASE_ENTERPRISE.TBA_DID is
'账号主键';

comment on column T_BASE_ENTERPRISE.TBE_DID is
'企业信息主键';

comment on column T_BASE_ENTERPRISE.TBE_NAME is
'企业名称';

comment on column T_BASE_ENTERPRISE.TBE_REMARK is
'备注';

comment on column T_BASE_ENTERPRISE.CREATER is
'创建人';

comment on column T_BASE_ENTERPRISE.CREATEDATE is
'创建时间';

comment on column T_BASE_ENTERPRISE.UPDATER is
'更新人';

comment on column T_BASE_ENTERPRISE.UPDATEDATE is
'更新时间';

-- Create sequence 
create sequence SEQ_T_BASE_VERIFICATION
minvalue 1
maxvalue 9999999999999999999
start with 21
increment by 1
cache 20
order;

-- Create sequence 
create sequence SEQ_T_BASE_ACCOUNT
minvalue 1
maxvalue 9999999999999999999
start with 21
increment by 1
cache 20
order;


 
/*==============================================================*/
/* Table: T_BASE_CUSTOMER                                       */
/*==============================================================*/
create table T_BASE_CUSTOMER 
(
   TSE_DID              INTEGER,
   TSC_DID              INTEGER              not null,
   TSC_NAME             nvarchar2(50),
   TSC_FAX              nvarchar2(50),
   TSC_EMAIL            nvarchar2(50),
   TSC_LINKMAN          nvarchar2(50),
   TSC_LINKTEL          nvarchar2(50),
   TSC_LINKMOBILE       nvarchar2(50),
   TSC_ADDER            nvarchar2(200),
   TSC_ZFB              nvarchar2(50),
   TSC_WCHAR            nvarchar2(50),
   TSC_BANKNAME         nvarchar2(50),
   TSC_BANK             nvarchar2(50),
   TSC_BANKCODE         nvarchar2(50),
   TSC_FAXHEARD         nvarchar2(50),
   TSC_STATUS           nvarchar2(50),
   TSC_REMARL           nvarchar2(500),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_CUSTOMER primary key (TSC_DID)
);

comment on table T_BASE_CUSTOMER is
'客户信息';

comment on column T_BASE_CUSTOMER.TSE_DID is
'企业信息主键';

comment on column T_BASE_CUSTOMER.TSC_DID is
'客户信息主键';

comment on column T_BASE_CUSTOMER.TSC_NAME is
'客户名称';

comment on column T_BASE_CUSTOMER.TSC_FAX is
'传真';

comment on column T_BASE_CUSTOMER.TSC_EMAIL is
'Email';

comment on column T_BASE_CUSTOMER.TSC_LINKMAN is
'联系人';

comment on column T_BASE_CUSTOMER.TSC_LINKTEL is
'联系电话';

comment on column T_BASE_CUSTOMER.TSC_LINKMOBILE is
'联系手机';

comment on column T_BASE_CUSTOMER.TSC_ADDER is
'地址';

comment on column T_BASE_CUSTOMER.TSC_ZFB is
'支付宝账号';

comment on column T_BASE_CUSTOMER.TSC_WCHAR is
'微信账号';

comment on column T_BASE_CUSTOMER.TSC_BANKNAME is
'开户名称';

comment on column T_BASE_CUSTOMER.TSC_BANK is
'开户银行';

comment on column T_BASE_CUSTOMER.TSC_BANKCODE is
'银行账号';

comment on column T_BASE_CUSTOMER.TSC_FAXHEARD is
'发票抬头';

comment on column T_BASE_CUSTOMER.TSC_STATUS is
'状态';

comment on column T_BASE_CUSTOMER.TSC_REMARL is
'备注';

comment on column T_BASE_CUSTOMER.CREATER is
'创建人';

comment on column T_BASE_CUSTOMER.CREATEDATE is
'创建时间';

comment on column T_BASE_CUSTOMER.UPDATER is
'更新人';

comment on column T_BASE_CUSTOMER.UPDATEDATE is
'更新时间';


/*==============================================================*/
/* Table: T_BASE_SUPPLIER                                         */
/*==============================================================*/
create table T_BASE_SUPPLIER 
(
   TSE_DID              INTEGER,
   TBS_DID              INTEGER              not null,
   TBS_NAME             nvarchar2(50),
   TBS_FAX              nvarchar2(50),
   TBS_EMAIL            nvarchar2(50),
   TBS_LINKMAN          nvarchar2(50),
   TBS_TEL              nvarchar2(50),
   TBS_MOBILE           nvarchar2(50),
   TBS_ADDER            nvarchar2(200),
   TBS_ZFB              nvarchar2(50),
   TBS_WCHAR            nvarchar2(50),
   TBS_BANKNAME         nvarchar2(50),
   TBS_BANK             nvarchar2(50),
   TBS_BANKCODE         nvarchar2(50),
   TBS_FAXHEARD         nvarchar2(50),
   TBS_STATUS           nvarchar2(50),
   TBS_REMARK           nvarchar2(500),
   CREATER              nvarchar2(50),
   CREATEDATE           DATE,
   UPDATER              nvarchar2(50),
   UPDATEDATE           DATE,
   constraint PK_T_BASE_SUPPLIER primary key (TBS_DID)
);

comment on table T_BASE_SUPPLIER is
'供应商信息';

comment on column T_BASE_SUPPLIER.TSE_DID is
'企业信息主键';

comment on column T_BASE_SUPPLIER.TBS_DID is
'供应商信息主键';

comment on column T_BASE_SUPPLIER.TBS_NAME is
'供应商名称';

comment on column T_BASE_SUPPLIER.TBS_FAX is
'传真';

comment on column T_BASE_SUPPLIER.TBS_EMAIL is
'Email';

comment on column T_BASE_SUPPLIER.TBS_LINKMAN is
'联系人';

comment on column T_BASE_SUPPLIER.TBS_TEL is
'联系电话';

comment on column T_BASE_SUPPLIER.TBS_MOBILE is
'联系手机';

comment on column T_BASE_SUPPLIER.TBS_ADDER is
'地址';

comment on column T_BASE_SUPPLIER.TBS_ZFB is
'支付宝账号';

comment on column T_BASE_SUPPLIER.TBS_WCHAR is
'微信账号';

comment on column T_BASE_SUPPLIER.TBS_BANKNAME is
'开户名称';

comment on column T_BASE_SUPPLIER.TBS_BANK is
'开户银行';

comment on column T_BASE_SUPPLIER.TBS_BANKCODE is
'银行账号';

comment on column T_BASE_SUPPLIER.TBS_FAXHEARD is
'发票抬头';

comment on column T_BASE_SUPPLIER.TBS_STATUS is
'状态';

comment on column T_BASE_SUPPLIER.TBS_REMARK is
'备注';

comment on column T_BASE_SUPPLIER.CREATER is
'创建人';

comment on column T_BASE_SUPPLIER.CREATEDATE is
'创建时间';

comment on column T_BASE_SUPPLIER.UPDATER is
'更新人';

comment on column T_BASE_SUPPLIER.UPDATEDATE is
'更新时间';

-- Create sequence 
create sequence SEQ_T_BASE_SUPPLIER
minvalue 1
maxvalue 9999999999999999999
start with 21
increment by 1
cache 20
order;

-- Create sequence 
create sequence SEQ_T_BASE_CUSTOMER
minvalue 1
maxvalue 9999999999999999999
start with 21
increment by 1
cache 20
order;


-- Create sequence 
create sequence SEQ_T_BASE_ENTERPRISE
minvalue 1
maxvalue 9999999999999999999
start with 1
increment by 1
cache 20
order;


-- Add/modify columns 
alter table T_BASE_GOODS add TBG_PRICE number;
-- Add comments to the columns 
comment on column T_BASE_GOODS.TBG_PRICE
  is '市场价格';

-- Add/modify columns 
alter table T_WMS_INDET add TWID_PRICE number;
alter table T_WMS_INDET add TWID_MONEY number;
-- Add comments to the columns 
comment on column T_WMS_INDET.TWID_PRICE
  is '单价';
comment on column T_WMS_INDET.TWID_MONEY
  is '金额';

  -- Add/modify columns 
alter table T_WMS_OUT add TWO_MONEY number;
alter table T_WMS_OUT add TWO_AGR nvarchar2(5);
-- Add comments to the columns 
comment on column T_WMS_OUT.TWO_MONEY
  is '应付金额';
comment on column T_WMS_OUT.TWO_AGR
  is '是否协议';

  -- Add/modify columns 
alter table T_WMS_ONDET add TWOD_PRICE number;
alter table T_WMS_ONDET add TWOD_MONEY number;
-- Add comments to the columns 
comment on column T_WMS_ONDET.TWOD_PRICE
  is '单价';
comment on column T_WMS_ONDET.TWOD_MONEY
  is '金额';

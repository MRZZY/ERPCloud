﻿using ERP.BLL.WMS;
using ERP.DAL;
using ERP.Model.WMS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.Web.Controllers
{
    public class ReportController : BaseController
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InOutDet()
        {
            if (this.Get("act") == "data")
            {
                Hashtable ht = new Hashtable();
                ht.Add("TBE_DID", this.BaseUserEnterpriseID);
                ht.Add("TBC_NAME", this.Post("TBC_NAME"));
                ht.Add("TBW_DID", this.Post("TBW_DID"));

                if (!string.IsNullOrEmpty(Post("DATE_LIKE")))
                {
                    ht.Add("DATE_BEGIN", DateTime.Parse(Post("DATE_LIKE").Split('~')[0].Trim()));
                    ht.Add("DATE_END", DateTime.Parse(Post("DATE_LIKE").Split('~')[1].Trim()));
                }

                ht.Add("NAME_SPEC", this.Post("NAME_SPEC"));

                DataTable dt = DalBase.objMapper.QueryForDataTable("wms.Select_InOutDet", ht);

                json.data = dt;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult InOutSum()
        {
            if (this.Get("act") == "data")
            {
                Hashtable ht = new Hashtable();
                ht.Add("TBE_DID", this.BaseUserEnterpriseID);
                ht.Add("DATE_BEGIN", DateTime.Parse(DateTime.Now.ToString("yyyy-MM")+"-01"));
                ht.Add("DATE_END", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")));

                DataTable dt = DalBase.objMapper.QueryForDataTable("wms.Select_InOutSum", ht);
                     
                DataTable dtCount = DalBase.objMapper.QueryForDataTable("wms.Select_InOutSumCount", ht);

                json.data = dt;
                json.data1 = dtCount;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

    }
}
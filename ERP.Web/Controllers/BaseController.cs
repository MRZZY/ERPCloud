﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using System.Net;
using ERP.Model.Base;
using ERP.BLL.Base;
using ERP.DAL;

namespace ERP.Web.Controllers
{
    public class BaseController : BasicsController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string ControllerInfo = RouteData.Values["controller"].ToString();
            string Action = RouteData.Values["action"].ToString();

            if (HttpContext != null)
            {                
                if (HttpContext.Request.Cookies["user"] == null)
                {
                    filterContext.Result = new RedirectResult("/home/login");
                }else if (HttpContext.Request.Cookies["EntUser"] == null
                    && Action != "Enterprise")
                {
                    filterContext.Result = new RedirectResult("/Essential/Enterprise");
                }
            }
            base.OnActionExecuting(filterContext);
        }
        
    }
}
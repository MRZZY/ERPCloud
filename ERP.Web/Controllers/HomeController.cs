﻿using ERP.BLL.Base;
using ERP.Model.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ERP.Web.Controllers
{
    public class HomeController : BasicsController
    {
        
        public ActionResult login()
        {
            if (this.Get("act") == "login")
            {
                json.status = 1;
                try
                {
                    string pas = Md5Hash(Post("PAS").Trim());

                    T_BASE_ACCOUNT tba = new T_BASE_ACCOUNT();
                    tba.TBA_PASSWORD = pas;
                    if (!string.IsNullOrEmpty(Post("ACCOUNT")))
                    {
                        if (Post("ACCOUNT").Contains("@"))
                        {
                            tba.TBA_EMAIL = Post("ACCOUNT");
                        }
                        else
                        {
                            tba.TBA_IPHONE = decimal.Parse(Post("ACCOUNT"));
                        }
                    }
                    IList<T_BASE_ACCOUNT> list = BllT_BASE_ACCOUNT.GetIList("SelectT_BASE_ACCOUNT_LOGIN", tba);
                    if (list.Count > 0)
                    {
                        HttpCookie user = new HttpCookie("user");//定义Cookie  
                        user["TBA_DID"] = HttpUtility.UrlEncode(list[0].TBA_DID.ToString());
                        user["TBA_NAME"] = HttpUtility.UrlEncode(list[0].TBA_NAME);
                        user["TBA_IPHONE"] = HttpUtility.UrlEncode(list[0].TBA_IPHONE.ToString());
                        user["TBA_EMAIL"] = HttpUtility.UrlEncode(list[0].TBA_EMAIL);
                        user["TBA_HPHOTO"] = string.IsNullOrEmpty(list[0].TBA_HPHOTO)? "../res/assets/images/users/2.jpg" : HttpUtility.UrlEncode(list[0].TBA_HPHOTO);
                        user.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(user);

                        if (bool.Parse(Post("REMEMBER")))
                        {
                            HttpCookie AccountName = new HttpCookie("AccountName");
                            AccountName.Value = Post("ACCOUNT");
                            Response.Cookies.Add(AccountName);
                        }
                        else
                        {
                            HttpCookie AccountName = new HttpCookie("AccountName");
                            AccountName.Expires = DateTime.Now.AddYears(-10);
                            System.Web.HttpContext.Current.Response.Cookies.Add(AccountName);
                        }

                        json.data = list;
                        json.status = 0;
                    }
                    else
                    {
                        json.message = "用户或密码错误";
                    }
                }
                catch (Exception ex)
                {
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "SendCode")
            {//发送验证码
                json.status = 1;
                try
                {
                    string Mail = Post("Mail").Trim();

                    //验证一分钟内是否发送过验证码
                    T_BASE_VERIFICATION tbv = new T_BASE_VERIFICATION();
                    tbv.TBV_MAIL = Mail;
                    tbv.CHECKMIN = 1;
                    IList<T_BASE_VERIFICATION> checkList = BllT_BASE_VERIFICATION.GetIList(tbv.SelectName, tbv);
                    if (checkList.Count() > 0)
                    {
                        json.message = "验证发送过于频繁，请稍后再操作";
                    }
                    else
                    {
                        if (Get("reg") == "reg")
                        {
                            string ErrCode = SendMail(Mail);
                            if (ErrCode != "")
                            {
                                json.message = ErrCode;
                            }
                            else
                            { json.status = 0; }
                        }
                        else
                        {
                            #region
                            //验证邮箱是否存在
                            T_BASE_ACCOUNT tab = new T_BASE_ACCOUNT();
                            tab.TBA_EMAIL = Mail;
                            IList<T_BASE_ACCOUNT> list = BllT_BASE_ACCOUNT.GetIList(tab.SelectName, tab);
                            if (list.Count() > 0)
                            {
                                string ErrCode = SendMail(Mail);
                                if (ErrCode != "")
                                {
                                    json.message = ErrCode;
                                }
                                else
                                { json.status = 0; }
                            }
                            else
                            {
                                json.message = "邮箱不存在";
                            }
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "CheckCode")
            {//验证验证码
                json.status = 1;
                try
                {
                    string Mail = Post("Mail").Trim();
                    string InCode = Post("InCode").Trim();

                    //验证三十分钟内是否发送过验证码
                    T_BASE_VERIFICATION tbv = new T_BASE_VERIFICATION();
                    tbv.TBV_MAIL = Mail;
                    tbv.TBV_CODE = InCode;
                    tbv.CHECKMIN = 30;
                    IList<T_BASE_VERIFICATION> checkList = BllT_BASE_VERIFICATION.GetIList(tbv.SelectName, tbv);
                    if (checkList.Count() > 0)
                    {
                        json.status = 0;
                    }
                    else
                    {
                        json.message = "验证错误或已过期";
                    }
                }
                catch (Exception ex)
                {
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "rePas")
            {//重置密码
                json.status = 1;
                try
                {
                    string Mail = Post("Mail").Trim();
                    string InCode = Post("InCode").Trim();
                    string NEWPAS = Post("NEWPAS").Trim();
                    //三小时内有验证码
                    T_BASE_VERIFICATION tbv = new T_BASE_VERIFICATION();
                    tbv.TBV_MAIL = Mail;
                    tbv.TBV_CODE = InCode;
                    tbv.CHECKMIN = 180;
                    IList<T_BASE_VERIFICATION> checkList = BllT_BASE_VERIFICATION.GetIList(tbv.SelectName, tbv);
                    if (checkList.Count() > 0)
                    {
                        T_BASE_ACCOUNT tba = new T_BASE_ACCOUNT();
                        tba.TBA_EMAIL = Mail;
                        IList<T_BASE_ACCOUNT> UList = BllT_BASE_ACCOUNT.GetIList(tba.SelectName, tba);
                        if (UList.Count() > 0)
                        {
                            tba = UList[0];
                            tba.TBA_PASSWORD= Md5Hash(NEWPAS);
                            BllT_BASE_ACCOUNT.UpdateData(tba);

                            json.status = 0;
                        }
                        else
                        {
                            json.message = "账号不存在";
                        }
                    }
                    else
                    {
                        json.message = "验证错误";
                    }
                }
                catch (Exception ex)
                {
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Register()
        {
            if (this.Get("act") == "Reg")
            {//注册
                json.status = 1;
                try
                {
                    string Mail = Post("Mail").Trim();
                    string InCode = Post("InCode").Trim();
                    string PAS = Post("PAS").Trim();
                    string InPhone = Post("InPhone").Trim();
                    string InName = Post("InName").Trim();

                    T_BASE_ACCOUNT tba = new T_BASE_ACCOUNT();
                    tba.TBA_EMAIL = Mail;
                    IList<T_BASE_ACCOUNT> tbaList = BllT_BASE_ACCOUNT.GetIList(tba.SelectName, tba);
                    if (tbaList.Count <= 0)
                    {
                        tba = new T_BASE_ACCOUNT();
                        tba.TBA_IPHONE = decimal.Parse(InPhone);
                        tbaList = BllT_BASE_ACCOUNT.GetIList(tba.SelectName, tba);
                        if (tbaList.Count <= 0)
                        {
                            //三小时内有验证码
                            T_BASE_VERIFICATION tbv = new T_BASE_VERIFICATION();
                            tbv.TBV_MAIL = Mail;
                            tbv.TBV_CODE = InCode;
                            tbv.CHECKMIN = 180;
                            IList<T_BASE_VERIFICATION> checkList = BllT_BASE_VERIFICATION.GetIList(tbv.SelectName, tbv);
                            if (checkList.Count() > 0)
                            {
                                tba = new T_BASE_ACCOUNT();
                                tba.TBA_EMAIL = Mail;
                                tba.TBA_NAME = InName;
                                tba.TBA_IPHONE = decimal.Parse(InPhone);
                                tba.TBA_PASSWORD = Md5Hash(PAS);
                                BllT_BASE_ACCOUNT.InsertData(tba);

                                HttpCookie AccountName = new HttpCookie("AccountName");
                                AccountName.Value = InPhone;
                                Response.Cookies.Add(AccountName);

                                json.status = 0;
                            }
                            else
                            {
                                json.message = "验证错误";
                            }
                        }
                        else
                        {
                            json.message = "手机已存在";
                        }
                    }
                    else {
                        json.message = "邮箱已存在";
                    }
                    
                }
                catch (Exception ex)
                {
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));

            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Index()
        {
            if (this.Get("act") == "menu")
            {

            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult About()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_USER ts = new T_BASE_USER();
                IList<T_BASE_USER> list = BllT_BASE_USER.GetIList(ts.SelectName, ts);


                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Home()
        {
            return View();
        }

    }
}
﻿using ERP.BLL.Base;
using ERP.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.Web.Controllers
{
    public class EssentialController : BaseController
    {
        // GET: Essential
        public ActionResult Index()
        {
            if (this.Get("act") == "menu")
            {

            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult UserOper()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_USER tbu = new T_BASE_USER();
                Base_SetObj(tbu);
                tbu.TBE_DID = this.BaseUserEnterpriseID;
                tbu.NOT_TBA_DID = 1;
                IList<T_BASE_USER> list = BllT_BASE_USER.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "state")
            {
                try
                {
                    T_BASE_USER tbu = new T_BASE_USER();
                    tbu.TBU_DID = decimal.Parse(Post("TBU_DID"));
                    IList<T_BASE_USER> list = BllT_BASE_USER.GetIList(tbu.SelectName, tbu);
                    if (list.Count > 0)
                    {
                        tbu = list[0];
                        Base_SetObj(tbu);
                        tbu.UPDATEID = this.BaseAccountID;
                        tbu.UPDATETIME = DateTime.Now;
                        BllT_BASE_USER.UpdateData(tbu);
                    }
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "SendMail")
            {
                T_BASE_ACCOUNT tba = new T_BASE_ACCOUNT();
                tba.TBA_EMAIL = Post("EMail");
                IList<T_BASE_ACCOUNT> tbaList = BllT_BASE_ACCOUNT.GetIList(tba.SelectName, tba);
                if (tbaList.Count <= 0)
                {
                    SendMail(Post("EMail"), "您的好友" + this.BaseAccountName + "，正在邀请您入住" + this.BaseUserEnterpriseName + ",<a href='http://localhost:52644/Home/register'>请点击此处</a>前往注册");
                    json.status = 1;//好友未注册
                }
                else
                {
                    T_BASE_USER tu = new T_BASE_USER();
                    tu.TBA_EMAIL = Post("EMail");
                    tu.TBE_DID = this.BaseUserEnterpriseID;
                    IList<T_BASE_USER> tuList = BllT_BASE_USER.GetIList(tu.SelectName, tu);
                    if (tuList.Count > 0)
                    {
                        json.status = 3;//该企业已存在该用户
                    }
                    else
                    {
                        string ErrCode = SendMail(Post("EMail"));
                        if (ErrCode != "")
                        {
                            json.message = ErrCode;
                            json.status = 2;//发送验证码错误
                        }
                        else
                        {
                            json.status = 0;
                        }
                    }
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    T_BASE_ACCOUNT tba = new T_BASE_ACCOUNT();
                    tba.TBA_EMAIL = Post("EMail");
                    IList<T_BASE_ACCOUNT> tbaList = BllT_BASE_ACCOUNT.GetIList(tba.SelectName, tba);
                    if (tbaList.Count > 0)
                    {
                        //验证三十分钟内是否发送过验证码
                        T_BASE_VERIFICATION tbv = new T_BASE_VERIFICATION();
                        tbv.TBV_MAIL = Post("EMail");
                        tbv.TBV_CODE = Post("CODE");
                        tbv.CHECKMIN = 30;
                        IList<T_BASE_VERIFICATION> checkList = BllT_BASE_VERIFICATION.GetIList(tbv.SelectName, tbv);
                        if (checkList.Count() <= 0)
                        {
                            json.status = 3;//验证错误或已过期
                        }
                        else
                        {
                            json.status = 0;
                            T_BASE_USER tuser = new T_BASE_USER();
                            tuser.TBA_DID = tbaList[0].TBA_DID;
                            tuser.TBE_DID = this.BaseUserEnterpriseID;
                            tuser.TBU_STATE = "0";
                            tuser.CREATEID = this.BaseAccountID;
                            tuser.CRREATETIME = DateTime.Now;
                            tuser.UPDATEID = this.BaseAccountID;
                            tuser.UPDATETIME = DateTime.Now;
                            BllT_BASE_USER.InsertData(tuser);
                        }
                    }
                    else
                    {
                        json.status = 2;
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Enterprise()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_USER tbe = new T_BASE_USER();
                tbe.TBA_DID = this.BaseAccountID;
                tbe.TBU_STATE = "0";
                IList<T_BASE_USER> list = BllT_BASE_USER.GetIList(tbe.SelectName, tbe);

                T_BASE_USER tbu = new T_BASE_USER();
                tbu.TBA_DID_ENT = this.BaseAccountID;
                IList<T_BASE_USER> tbu_list = BllT_BASE_USER.GetIList(tbu.SelectName, tbu);
                if (tbu_list.Count > 0)
                {
                    foreach (T_BASE_USER detTbe in list)
                    {
                        foreach (T_BASE_USER detUser in tbu_list)
                        {
                            if (detUser.TBE_DID == detTbe.TBE_DID)
                            {
                                detTbe.All_USER += "   " + detUser.TBA_NAME;
                            }
                        }

                        detTbe.All_USER = detTbe.All_USER == null ? "" : detTbe.All_USER.Trim();
                    }
                }

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    T_BASE_ENTERPRISE tbu = new T_BASE_ENTERPRISE();
                    if (!string.IsNullOrEmpty(Post("TBU_DID")))
                    {//修改                        
                        tbu.TBE_DID = decimal.Parse(Post("TBU_DID"));
                        IList<T_BASE_ENTERPRISE> list = BllT_BASE_ENTERPRISE.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            tbu = list[0];
                            Base_SetObj(tbu);
                            BllT_BASE_ENTERPRISE.UpdateData(tbu);
                        }
                    }
                    else
                    {//新增
                        Base_SetObj(tbu);
                        tbu.TBA_DID = this.BaseAccountID;
                        tbu.CREATER = this.BaseAccountID.ToString();
                        tbu.CREATEDATE = DateTime.Now;
                        tbu.UPDATER = this.BaseAccountID.ToString();
                        tbu.UPDATEDATE = DateTime.Now;
                        decimal EntId = BllT_BASE_ENTERPRISE.InsertData(tbu);

                        #region 往该企业插入用户信息
                        T_BASE_USER tuser = new T_BASE_USER();
                        tuser.TBA_DID = this.BaseAccountID;
                        tuser.TBE_DID = EntId;
                        tuser.TBU_STATE = "0";
                        //tuser.CREATEID = this.BaseUserID;
                        tuser.CRREATETIME = DateTime.Now;
                        //tuser.UPDATEID = this.BaseUserID;
                        tuser.UPDATETIME = DateTime.Now;
                        BllT_BASE_USER.InsertData(tuser);

                        T_BASE_WAREHOUSE tw = new T_BASE_WAREHOUSE();
                        tw.TSE_DID = EntId;
                        tw.TBW_NAME = "默认仓库";
                        tw.TBW_STATUS = "0";
                        BllT_BASE_WAREHOUSE.InsertData(tw);

                        string[] sl = new string[] { "个", "件", "盒", "条", "袋", "包" };
                        foreach (string sde in sl)
                        {
                            T_BASE_UNIT tun = new T_BASE_UNIT();
                            tun.TSE_DID = EntId;
                            tun.TBU_NAME = sde;
                            BllT_BASE_UNIT.InsertData(tun);
                        }
                        #endregion
                    }
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "EntUser")
            {
                HttpCookie user = new HttpCookie("EntUser");//定义Cookie  
                user["TBU_DID"] = this.Post("TBU_DID");
                user["TBE_DID"] = this.Post("TBE_DID");
                user["TBE_NAME"] = this.Post("TBE_NAME");
                user.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(user);

                json.status = 0;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Customer()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_CUSTOMER tbu = new T_BASE_CUSTOMER();
                Base_SetObj(tbu);
                tbu.TSC_STATUS = tbu.TSC_STATUS_LIKE == "true" ? "0" : "";
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_CUSTOMER> list = BllT_BASE_CUSTOMER.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "selectData")
            {
                T_BASE_CUSTOMER tbu = new T_BASE_CUSTOMER();
                Base_SetObj(tbu);
                tbu.TSC_STATUS = "0";
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_CUSTOMER> list = BllT_BASE_CUSTOMER.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_CUSTOMER.DeleteData(int.Parse(Post("TSC_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_CUSTOMER tbu = new T_BASE_CUSTOMER();
                    if (!string.IsNullOrEmpty(Post("TSC_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TSC_NAME = this.Post("TSC_NAME");
                        IList<T_BASE_CUSTOMER> list = BllT_BASE_CUSTOMER.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_CUSTOMER det in list)
                        {
                            if (det.TSC_DID.ToString() != Post("TSC_DID"))
                            {
                                json.status = 1;
                                json.message = "客户名称已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_CUSTOMER();
                            tbu.TSC_DID = decimal.Parse(Post("TSC_DID"));
                            list = BllT_BASE_CUSTOMER.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_CUSTOMER.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TSC_NAME = this.Post("TSC_NAME");
                        IList<T_BASE_CUSTOMER> list = BllT_BASE_CUSTOMER.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "客户名称已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_CUSTOMER();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.TSC_STATUS = "0";
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_CUSTOMER.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Supplier()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_SUPPLIER tbu = new T_BASE_SUPPLIER();
                Base_SetObj(tbu);
                tbu.TBS_STATUS = tbu.TBS_STATUS_LIKE == "true" ? "0" : "";
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_SUPPLIER> list = BllT_BASE_SUPPLIER.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "selectData")
            {
                T_BASE_SUPPLIER tbu = new T_BASE_SUPPLIER();
                Base_SetObj(tbu);
                tbu.TBS_STATUS = "0";
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_SUPPLIER> list = BllT_BASE_SUPPLIER.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_SUPPLIER.DeleteData(int.Parse(Post("TBS_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_SUPPLIER tbu = new T_BASE_SUPPLIER();
                    if (!string.IsNullOrEmpty(Post("TBS_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBS_NAME = this.Post("TBS_NAME");
                        IList<T_BASE_SUPPLIER> list = BllT_BASE_SUPPLIER.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_SUPPLIER det in list)
                        {
                            if (det.TBS_DID.ToString() != Post("TBS_DID"))
                            {
                                json.status = 1;
                                json.message = "客户名称已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_SUPPLIER();
                            tbu.TBS_DID = decimal.Parse(Post("TBS_DID"));
                            list = BllT_BASE_SUPPLIER.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_SUPPLIER.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBS_NAME = this.Post("TBS_NAME");
                        IList<T_BASE_SUPPLIER> list = BllT_BASE_SUPPLIER.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "客户名称已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_SUPPLIER();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.TBS_STATUS = "0";
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_SUPPLIER.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Warehouse()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_WAREHOUSE tbu = new T_BASE_WAREHOUSE();
                Base_SetObj(tbu);
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_WAREHOUSE> list = BllT_BASE_WAREHOUSE.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_WAREHOUSE.DeleteData(int.Parse(Post("TBW_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_WAREHOUSE tbu = new T_BASE_WAREHOUSE();
                    if (!string.IsNullOrEmpty(Post("TBW_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBW_NAME = this.Post("TBW_NAME");
                        IList<T_BASE_WAREHOUSE> list = BllT_BASE_WAREHOUSE.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_WAREHOUSE det in list)
                        {
                            if (det.TBW_DID.ToString() != Post("TBW_DID"))
                            {
                                json.status = 1;
                                json.message = "仓库名称已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_WAREHOUSE();
                            tbu.TBW_DID = decimal.Parse(Post("TBW_DID"));
                            list = BllT_BASE_WAREHOUSE.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_WAREHOUSE.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBW_NAME = this.Post("TBW_NAME");
                        IList<T_BASE_WAREHOUSE> list = BllT_BASE_WAREHOUSE.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "仓库名称已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_WAREHOUSE();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.TBW_STATUS = "0";
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_WAREHOUSE.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Unit()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_UNIT tbu = new T_BASE_UNIT();
                Base_SetObj(tbu);
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_UNIT> list = BllT_BASE_UNIT.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_UNIT.DeleteData(int.Parse(Post("TBU_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_UNIT tbu = new T_BASE_UNIT();
                    if (!string.IsNullOrEmpty(Post("TBU_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBU_NAME = this.Post("TBU_NAME");
                        IList<T_BASE_UNIT> list = BllT_BASE_UNIT.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_UNIT det in list)
                        {
                            if (det.TBU_DID.ToString() != Post("TBU_DID"))
                            {
                                json.status = 1;
                                json.message = "仓库名称已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_UNIT();
                            tbu.TBU_DID = decimal.Parse(Post("TBU_DID"));
                            list = BllT_BASE_UNIT.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_UNIT.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBU_NAME = this.Post("TBU_NAME");
                        IList<T_BASE_UNIT> list = BllT_BASE_UNIT.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "仓库名称已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_UNIT();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_UNIT.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Categories()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_CATEGORIES tbu = new T_BASE_CATEGORIES();
                Base_SetObj(tbu);
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_CATEGORIES> list = BllT_BASE_CATEGORIES.GetIList(tbu.SelectName, tbu);

                json.data = CateJson(list, 0, -1).Trim().Trim(',');
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_CATEGORIES.DeleteData(int.Parse(Post("TBC_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_CATEGORIES tbu = new T_BASE_CATEGORIES();
                    if (!string.IsNullOrEmpty(Post("TBC_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBC_NAME = this.Post("TBC_NAME");
                        IList<T_BASE_CATEGORIES> list = BllT_BASE_CATEGORIES.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_CATEGORIES det in list)
                        {
                            if (det.TBC_DID.ToString() != Post("TBC_DID"))
                            {
                                json.status = 1;
                                json.message = "分类名称已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_CATEGORIES();
                            tbu.TBC_DID = decimal.Parse(Post("TBC_DID"));
                            list = BllT_BASE_CATEGORIES.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_CATEGORIES.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBC_NAME = this.Post("TBC_NAME");
                        IList<T_BASE_CATEGORIES> list = BllT_BASE_CATEGORIES.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "分类名称已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_CATEGORIES();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_CATEGORIES.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        int nid = 0;
        private string CateJson(IList<T_BASE_CATEGORIES> list, decimal Parent, int pnid)
        {
            string Json = "";

            List<T_BASE_CATEGORIES> Rootlist = list.Where(a => a.TBC_PARENT == Parent).ToList<T_BASE_CATEGORIES>();
            if (Rootlist.Count > 0)
            {
                if (Parent != 0)
                {
                    Json += ",\"nodes\": [";
                }
                foreach (T_BASE_CATEGORIES node in Rootlist)
                {
                    string NodeStr = node.TBC_PARENT + ",\\\"" + node.TBC_PARENT_NAME + "\\\"," + node.TBC_DID + ",\\\"" + node.TBC_NAME + "\\\"," + pnid;
                    Json += "{\"nid\":" + nid + ",\"id\":\"" + node.TBC_DID + "\",\"text\": \"" + node.TBC_NAME + "\",\"HtmlStr\": \" <a onClick='vm.OpenMode(" + NodeStr + ")'><i class='fa fa-plus'></i> 修改</a>    <a onClick='vm.Delete(" + node.TBC_DID + ")'><i class='fa fa-plus'></i> 删除</a>\"";
                    nid++;
                    Json += CateJson(list, node.TBC_DID, nid - 1);
                    Json += "},";
                }
                if (Parent != 0)
                {
                    Json = Json.Trim().TrimEnd(',') + "]";
                }
            }

            return Json;
        }

        public ActionResult Goods()
        {
            if (this.Get("act") == "data")
            {
                T_BASE_GOODS tbu = new T_BASE_GOODS();
                Base_SetObj(tbu);
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                tbu.TBG_STATUS = tbu.TBG_STATUS_LIKE == "true" ? "0" : "";
                IList<T_BASE_GOODS> list = BllT_BASE_GOODS.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "selectData")
            {
                T_BASE_GOODS tbu = new T_BASE_GOODS();
                Base_SetObj(tbu);
                tbu.TBG_STATUS = "0";
                tbu.TSE_DID = this.BaseUserEnterpriseID;
                IList<T_BASE_GOODS> list = BllT_BASE_GOODS.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                try
                {
                    BllT_BASE_GOODS.DeleteData(int.Parse(Post("TBG_DID")));
                    json.status = 0;
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                try
                {
                    json.status = 0;

                    T_BASE_GOODS tbu = new T_BASE_GOODS();
                    if (!string.IsNullOrEmpty(Post("TBG_DID")))
                    {   //修改
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBG_NAME = this.Post("TBG_NAME");
                        tbu.TBG_SPEC = this.Post("TBG_SPEC");
                        IList<T_BASE_GOODS> list = BllT_BASE_GOODS.GetIList(tbu.SelectName, tbu);
                        foreach (T_BASE_GOODS det in list)
                        {
                            if (det.TBG_DID.ToString() != Post("TBG_DID"))
                            {
                                json.status = 1;
                                json.message = "商品名称+规格已存在";
                            }
                        }

                        if (json.status == 0)
                        {
                            tbu = new T_BASE_GOODS();
                            tbu.TBG_DID = decimal.Parse(Post("TBG_DID"));
                            list = BllT_BASE_GOODS.GetIList(tbu.SelectName, tbu);
                            if (list.Count > 0)
                            {
                                tbu = list[0];
                                Base_SetObj(tbu);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_BASE_GOODS.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {//新增
                        tbu.TSE_DID = this.BaseAccountID;
                        tbu.TBG_NAME = this.Post("TBG_NAME");
                        tbu.TBG_SPEC = this.Post("TBG_SPEC");
                        IList<T_BASE_GOODS> list = BllT_BASE_GOODS.GetIList(tbu.SelectName, tbu);
                        if (list.Count > 0)
                        {
                            json.status = 1;
                            json.message = "商品名称+规格已存在";
                        }
                        else
                        {
                            tbu = new T_BASE_GOODS();
                            Base_SetObj(tbu);
                            tbu.TSE_DID = this.BaseUserEnterpriseID;
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            BllT_BASE_GOODS.InsertData(tbu);
                            json.status = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult Settings()
        {
            if (this.Get("act") == "UpU")
            {
                try
                {
                    json.status = 0;
                    T_BASE_ACCOUNT tbu = new T_BASE_ACCOUNT();
                    tbu.TBA_DID = this.BaseAccountID;
                    IList<T_BASE_ACCOUNT> list = BllT_BASE_ACCOUNT.GetIList(tbu.SelectName, tbu);
                    if (list.Count > 0)
                    {
                        tbu = list[0];
                        Base_SetObj(tbu);
                        BllT_BASE_ACCOUNT.UpdateData(tbu);

                        HttpCookie user = new HttpCookie("user");//定义Cookie  
                        user["TBA_DID"] = HttpUtility.UrlEncode(tbu.TBA_DID.ToString());
                        user["TBA_NAME"] = HttpUtility.UrlEncode(tbu.TBA_NAME);
                        user["TBA_IPHONE"] = HttpUtility.UrlEncode(tbu.TBA_IPHONE.ToString());
                        user["TBA_EMAIL"] = HttpUtility.UrlEncode(tbu.TBA_EMAIL);
                        user["TBA_HPHOTO"] = HttpUtility.UrlEncode(tbu.TBA_HPHOTO);
                        user.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(user);
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "UpPas")
            {
                try
                {
                    json.status = 0;
                    T_BASE_ACCOUNT tbu = new T_BASE_ACCOUNT();
                    tbu.TBA_DID = this.BaseAccountID;
                    IList<T_BASE_ACCOUNT> list = BllT_BASE_ACCOUNT.GetIList(tbu.SelectName, tbu);
                    if (list.Count > 0)
                    {
                        tbu = list[0];
                        tbu.TBA_PASSWORD = this.Md5Hash(Post("PAS"));
                        BllT_BASE_ACCOUNT.UpdateData(tbu);
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "UpHp")
            {
                try
                {
                    json.status = 0;
                    this.Base64Img(this.Post("img"), "/res/image/user/", this.BaseAccountID + ".jpg");

                    T_BASE_ACCOUNT tbu = new T_BASE_ACCOUNT();
                    tbu.TBA_DID = this.BaseAccountID;
                    IList<T_BASE_ACCOUNT> list = BllT_BASE_ACCOUNT.GetIList(tbu.SelectName, tbu);
                    if (list.Count > 0)
                    {
                        tbu = list[0];
                        tbu.TBA_HPHOTO = "../res/image/user/" + this.BaseAccountID + ".jpg";
                        BllT_BASE_ACCOUNT.UpdateData(tbu);
                    }
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }



    }
}
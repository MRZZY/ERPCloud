﻿using ERP.BLL.WMS;
using ERP.Model.WMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace ERP.Web.Controllers
{
    public class WMSController : BaseController
    {
        // GET: WMS
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult InStock()
        {
            if (this.Get("act") == "data")
            {
                T_WMS_IN tbu = new T_WMS_IN();
                Base_SetObj(tbu);
                if (!string.IsNullOrEmpty(Post("TWI_DATE_LIKE")))
                {
                    tbu.TWI_DATE_BEGIN = DateTime.Parse(Post("TWI_DATE_LIKE").Split('~')[0].Trim());
                    tbu.TWI_DATE_END = DateTime.Parse(Post("TWI_DATE_LIKE").Split('~')[1].Trim());
                }
                tbu.TBE_DID = this.BaseUserEnterpriseID;
                IList<T_WMS_IN> list = BllT_WMS_IN.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "detData")
            {
                T_WMS_INDET tbu = new T_WMS_INDET();
                Base_SetObj(tbu);
                IList<T_WMS_INDET> list = BllT_WMS_INDET.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                this.IBeginTransaction();
                try
                {
                    #region
                    //库存更新集合
                    List<T_WMS_STOCK> upList = new List<T_WMS_STOCK>();

                    T_WMS_INDET detData = new T_WMS_INDET();
                    detData.TWI_DID = decimal.Parse(Post("TWI_DID"));
                    IList<T_WMS_INDET> detList = BllT_WMS_INDET.GetIList(detData.SelectName, detData);
                    foreach (T_WMS_INDET det in detList)
                    {
                        List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == det.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                        if (Cl.Count() > 0)
                        {
                            Cl[0].TWID_NUM = Cl[0].TWID_NUM - det.TWID_NUM;
                        }
                        else
                        {
                            T_WMS_STOCK stk = new T_WMS_STOCK();
                            stk.TBE_DID = this.BaseUserEnterpriseID;
                            stk.TBW_DID = det.TBW_DID;//修改原数据按库内数据
                            stk.TBG_DID = det.TBG_DID;
                            stk.TWID_NUM = 0 - det.TWID_NUM;
                            upList.Add(stk);
                        }
                    }
                    UpdateStock(upList);
                    #endregion

                    BllT_WMS_IN.DeleteData(int.Parse(Post("TWI_DID")));
                    BllT_WMS_INDET.DeleteDataAll(decimal.Parse(Post("TWI_DID")));
                    json.status = 0;

                    this.ICommitTransaction();
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                    this.IRollBackTransaction();
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                json.status = 0;

                this.IBeginTransaction();

                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();   //实例化一个能够序列化数据的类
                    T_WMS_IN tbu = js.Deserialize<T_WMS_IN>(Post("DStr"));    //将json数据转化为对象类型并赋值给list

                    T_WMS_INDET detData = new T_WMS_INDET();
                    IList<T_WMS_INDET> detList = null;

                    T_WMS_IN CTbu = new T_WMS_IN();
                    CTbu.TWI_DOC = tbu.TWI_DOC;
                    CTbu.TBE_DID = this.BaseAccountID;
                    IList<T_WMS_IN> Clist = BllT_WMS_IN.GetIList(CTbu.SelectName, CTbu);

                    //T_WMS_IN tbu = new T_WMS_IN();
                    if (tbu.TWI_DID != 0)
                    {
                        foreach (T_WMS_IN det in Clist)
                        {
                            if (det.TWI_DID != tbu.TWI_DID)
                            {
                                json.status = 1;
                                json.message = "入库单号已存在";
                            }
                        }
                        if (json.status == 0)
                        {
                            //修改
                            T_WMS_IN OldTbu = new T_WMS_IN();
                            OldTbu.TWI_DID = tbu.TWI_DID;
                            IList<T_WMS_IN> list = BllT_WMS_IN.GetIList(OldTbu.SelectName, OldTbu);
                            if (list.Count > 0)
                            {
                                detData = new T_WMS_INDET();
                                detData.TWI_DID = tbu.TWI_DID;
                                detList = BllT_WMS_INDET.GetIList(detData.SelectName, detData);

                                tbu.TWI_DID = list[0].TWI_DID;
                                tbu.CREATER = list[0].CREATER;
                                tbu.CREATEDATE = list[0].CREATEDATE;

                                tbu.TWI_DATE = DateTime.Parse(tbu.TWI_DATE_F);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_WMS_IN.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {
                        if (Clist.Count > 0)
                        {
                            json.status = 1;
                            json.message = "入库单号已存在";
                        }
                        else
                        {
                            //新增
                            tbu.TWI_DATE = DateTime.Parse(tbu.TWI_DATE_F);
                            tbu.TBE_DID = this.BaseUserEnterpriseID;
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            tbu.TWI_DID = BllT_WMS_IN.InsertData(tbu);
                        }
                    }

                    //库存更新集合
                    List<T_WMS_STOCK> upList = new List<T_WMS_STOCK>();

                    if (detList != null)
                    {
                        foreach (T_WMS_INDET det in detList)
                        {
                            List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == det.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                            if (Cl.Count() > 0)
                            {
                                Cl[0].TWID_NUM = Cl[0].TWID_NUM - det.TWID_NUM;
                            }
                            else
                            {
                                T_WMS_STOCK stk = new T_WMS_STOCK();
                                stk.TBE_DID = this.BaseUserEnterpriseID;
                                stk.TBW_DID = det.TBW_DID;//修改原数据按库内数据
                                stk.TBG_DID = det.TBG_DID;
                                stk.TWID_NUM = 0 - det.TWID_NUM;
                                upList.Add(stk);
                            }
                        }
                    }

                    BllT_WMS_INDET.DeleteDataAll(tbu.TWI_DID);

                    foreach (T_WMS_INDET det in tbu.detList)
                    {
                        det.TWI_DID = tbu.TWI_DID;

                        List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == tbu.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                        if (Cl.Count() > 0)
                        {
                            Cl[0].TWID_NUM = Cl[0].TWID_NUM + det.TWID_NUM;
                        }
                        else
                        {
                            T_WMS_STOCK stk = new T_WMS_STOCK();
                            stk.TBE_DID = this.BaseUserEnterpriseID;
                            stk.TBW_DID = tbu.TBW_DID;//新增/修改仓库按界面上值
                            stk.TBG_DID = det.TBG_DID;
                            stk.TWID_NUM = det.TWID_NUM;
                            upList.Add(stk);
                        }
                    }
                    BllT_WMS_INDET.InsertData(tbu.detList);
                    UpdateStock(upList);
                    this.ICommitTransaction();
                }
                catch (Exception ex)
                {
                    this.IRollBackTransaction();
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult outStock()
        {
            if (this.Get("act") == "data")
            {
                T_WMS_OUT tbu = new T_WMS_OUT();
                Base_SetObj(tbu);
                if (!string.IsNullOrEmpty(Post("TWO_DATE_LIKE")))
                {
                    tbu.TWO_DATE_BEGIN = DateTime.Parse(Post("TWO_DATE_LIKE").Split('~')[0].Trim());
                    tbu.TWO_DATE_END = DateTime.Parse(Post("TWO_DATE_LIKE").Split('~')[1].Trim());
                }
                tbu.TBE_DID = this.BaseUserEnterpriseID;
                IList<T_WMS_OUT> list = BllT_WMS_OUT.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "detData")
            {
                T_WMS_ONDET tbu = new T_WMS_ONDET();
                Base_SetObj(tbu);
                IList<T_WMS_ONDET> list = BllT_WMS_ONDET.GetIList(tbu.SelectName, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Post("act") == "delete")
            {
                this.IBeginTransaction();
                try
                {
                    #region
                    //库存更新集合
                    List<T_WMS_STOCK> upList = new List<T_WMS_STOCK>();

                    T_WMS_ONDET detData = new T_WMS_ONDET();
                    detData.TWO_DID = decimal.Parse(Post("TWO_DID"));
                    IList<T_WMS_ONDET> detList = BllT_WMS_ONDET.GetIList(detData.SelectName, detData);
                    foreach (T_WMS_ONDET det in detList)
                    {
                        List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == det.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                        if (Cl.Count() > 0)
                        {
                            Cl[0].TWID_NUM = Cl[0].TWID_NUM + det.TWID_NUM;
                        }
                        else
                        {
                            T_WMS_STOCK stk = new T_WMS_STOCK();
                            stk.TBE_DID = this.BaseUserEnterpriseID;
                            stk.TBW_DID = det.TBW_DID;//修改原数据按库内数据
                            stk.TBG_DID = det.TBG_DID;
                            stk.TWID_NUM = det.TWID_NUM;
                            upList.Add(stk);
                        }
                    }
                    UpdateStock(upList);
                    #endregion

                    BllT_WMS_OUT.DeleteData(int.Parse(Post("TWO_DID")));
                    BllT_WMS_ONDET.DeleteDataAll(decimal.Parse(Post("TWO_DID")));
                    json.status = 0;

                    this.ICommitTransaction();
                }
                catch (Exception ex)
                {
                    json.status = 1;
                    json.message = ex.Message;
                    this.IRollBackTransaction();
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else if (this.Get("act") == "save")
            {
                json.status = 0;

                this.IBeginTransaction();

                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();   //实例化一个能够序列化数据的类
                    T_WMS_OUT tbu = js.Deserialize<T_WMS_OUT>(Post("DStr"));    //将json数据转化为对象类型并赋值给list
                    tbu.TWO_AGR_T = tbu.TWO_AGR.ToString();
                    T_WMS_ONDET detData = new T_WMS_ONDET();
                    IList<T_WMS_ONDET> detList =null;

                    T_WMS_OUT CTbu = new T_WMS_OUT();
                    CTbu.TWO_DOC = tbu.TWO_DOC;
                    CTbu.TBE_DID = this.BaseAccountID;
                    IList<T_WMS_OUT> Clist = BllT_WMS_OUT.GetIList(CTbu.SelectName, CTbu);

                    //T_WMS_IN tbu = new T_WMS_IN();
                    if (tbu.TWO_DID != 0)
                    {
                        foreach (T_WMS_OUT det in Clist)
                        {
                            if (det.TWO_DID != tbu.TWO_DID)
                            {
                                json.status = 1;
                                json.message = "入库单号已存在";
                            }
                        }
                        if (json.status == 0)
                        {
                            //修改
                            T_WMS_OUT OldTbu = new T_WMS_OUT();
                            OldTbu.TWO_DID = tbu.TWO_DID;
                            IList<T_WMS_OUT> list = BllT_WMS_OUT.GetIList(OldTbu.SelectName, OldTbu);
                            if (list.Count > 0)
                            {
                                detData.TWO_DID = tbu.TWO_DID;
                                detList = BllT_WMS_ONDET.GetIList(detData.SelectName, detData);
                                
                                tbu.TWO_DID = list[0].TWO_DID;
                                tbu.CREATER = list[0].CREATER;
                                tbu.CREATEDATE = list[0].CREATEDATE;

                                tbu.TWO_DATE = DateTime.Parse(tbu.TWO_DATE_F);
                                tbu.UPDATER = this.BaseAccountID.ToString();
                                tbu.UPDATEDATE = DateTime.Now;
                                BllT_WMS_OUT.UpdateData(tbu);
                            }
                        }
                    }
                    else
                    {
                        if (Clist.Count > 0)
                        {
                            json.status = 1;
                            json.message = "入库单号已存在";
                        }
                        else
                        {
                            //新增
                            tbu.TWO_DATE = DateTime.Parse(tbu.TWO_DATE_F);
                            tbu.TBE_DID = this.BaseUserEnterpriseID;
                            tbu.CREATER = this.BaseAccountID.ToString();
                            tbu.CREATEDATE = DateTime.Now;
                            tbu.UPDATER = this.BaseAccountID.ToString();
                            tbu.UPDATEDATE = DateTime.Now;
                            tbu.TWO_DID = BllT_WMS_OUT.InsertData(tbu);
                        }
                    }

                    //库存更新集合
                    List<T_WMS_STOCK> upList = new List<T_WMS_STOCK>();
                    if (detList != null)
                    {
                        foreach (T_WMS_ONDET det in detList)
                        {
                            List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == det.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                            if (Cl.Count() > 0)
                            {
                                Cl[0].TWID_NUM = Cl[0].TWID_NUM + det.TWID_NUM;
                            }
                            else
                            {
                                T_WMS_STOCK stk = new T_WMS_STOCK();
                                stk.TBE_DID = this.BaseUserEnterpriseID;
                                stk.TBW_DID = det.TBW_DID;//修改原数据按库内数据
                                stk.TBG_DID = det.TBG_DID;
                                stk.TWID_NUM = det.TWID_NUM;
                                upList.Add(stk);
                            }
                        }
                    }

                    foreach (T_WMS_ONDET det in tbu.detList)
                    {
                        det.TWO_DID = tbu.TWO_DID;

                        List<T_WMS_STOCK> Cl = upList.Where(e => e.TBW_DID == tbu.TBW_DID && e.TBG_DID == det.TBG_DID).ToList<T_WMS_STOCK>();
                        if (Cl.Count() > 0)
                        {
                            Cl[0].TWID_NUM = Cl[0].TWID_NUM - det.TWID_NUM;
                        }
                        else
                        {
                            T_WMS_STOCK stk = new T_WMS_STOCK();
                            stk.TBE_DID = this.BaseUserEnterpriseID;
                            stk.TBW_DID = tbu.TBW_DID;//新增/修改仓库按界面上值
                            stk.TBG_DID = det.TBG_DID;
                            stk.TWID_NUM = 0 - det.TWID_NUM;
                            upList.Add(stk);
                        }
                    }
                    BllT_WMS_ONDET.DeleteDataAll(tbu.TWO_DID);
                    BllT_WMS_ONDET.InsertData(tbu.detList);
                    UpdateStock(upList);
                    this.ICommitTransaction();
                }
                catch (Exception ex)
                {
                    this.IRollBackTransaction();
                    json.status = 1;
                    json.message = ex.Message;
                }
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }

        public ActionResult StockInfo()
        {
            if (this.Get("act") == "data")
            {
                T_WMS_STOCK tbu = new T_WMS_STOCK();
                Base_SetObj(tbu);

                if (!string.IsNullOrEmpty(Post("NCheck")))
                {
                    tbu.NOT_ZERO = Post("NCheck") == "true" ? 1 : 0;
                }
                tbu.TBE_DID = this.BaseUserEnterpriseID;
                IList<T_WMS_STOCK> list = BllT_WMS_STOCK.GetIList(tbu.SelectNamePage, tbu);

                json.data = list;
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }


        public ActionResult OutPrint()
        {
            if (this.Get("act") == "data")
            {
                T_WMS_OUT tbu = new T_WMS_OUT();
                tbu.TWO_DID =decimal.Parse(this.Post("TWO_DID"));
                tbu.TBE_DID = this.BaseUserEnterpriseID;
                IList<T_WMS_OUT> list = BllT_WMS_OUT.GetIList(tbu.SelectNamePage, tbu);

                if (list.Count > 0)
                {
                    tbu= list[0];
                    T_WMS_ONDET tbuD = new T_WMS_ONDET();
                    tbuD.TWO_DID = tbu.TWO_DID;
                    IList<T_WMS_ONDET> listD = BllT_WMS_ONDET.GetIList(tbuD.SelectName, tbuD);
                    json.data1 = listD;
                    if (tbu.TWO_AGR)
                    {
                        tbu.SUM_MONEY = listD.Sum(a => a.TWOD_MONEY);
                        tbu.DISCOUNT = tbu.SUM_MONEY - tbu.TWO_MONEY;                        
                    }
                    tbu.TWO_DATE_T = tbu.TWO_DATE.ToString("yyyy年MM月dd日");
                    json.data = tbu;
                }

                
                ReturnView = Content(Base.Json.JsonUtils.Serialize(json));
            }
            else
            {
                ReturnView = View();
            }
            return ReturnView;
        }



        #region 更新库存

        private void UpdateStock(List<T_WMS_STOCK> upList)
        {
            List<T_WMS_STOCK> EditList = new List<T_WMS_STOCK>();
            List<T_WMS_STOCK> InsrList = new List<T_WMS_STOCK>();

            T_WMS_STOCK findWms = new T_WMS_STOCK();
            foreach (T_WMS_STOCK det in upList)
            {
                findWms.TBE_DID = det.TBE_DID;
                findWms.TBW_DID = det.TBW_DID;
                findWms.TBG_DID = det.TBG_DID;
                IList<T_WMS_STOCK> findList = BllT_WMS_STOCK.GetIList(findWms.SelectName, findWms);
                if (findList.Count > 0)
                {
                    EditList.Add(det);
                }
                else
                {
                    InsrList.Add(det);
                }
            }
            if (InsrList.Count > 0)
            {
                BllT_WMS_STOCK.InsertData(InsrList);
            }
            if (EditList.Count > 0)
            {
                BllT_WMS_STOCK.UpStockData(EditList);
            }
        }

        #endregion


    }
}
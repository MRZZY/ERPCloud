﻿
function ShowMsg(msgTitle, callback)
{
    swal({
        title: "确定" + msgTitle + "?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "是",
        cancelButtonText: "否",
        closeOnConfirm: false,
        closeOnCancel: true
    }, callback);
}

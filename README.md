 **进销存云平台** 

功能介绍
商品分类、商品信息、客户管理、供应商管理、入库单、出库单、库存查询、出入库查询

系统为云平台，注册后可进入
开发语言.net mvc
数据库oracle
可支持定制开发

 **最新版本 .net core2.2 数据库 mysql** 

系统展示

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/093700_a2780afc_1365116.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/093739_72e9e145_1365116.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/093817_1d93d7dc_1365116.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/093906_dcf55987_1365116.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/093844_08ef89a6_1365116.png "屏幕截图.png")
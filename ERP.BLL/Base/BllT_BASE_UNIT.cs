﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_UNIT 
   {
        public static IList<T_BASE_UNIT> GetIList(string SelectName,T_BASE_UNIT model)
        {
            return DalT_BASE_UNIT.GetIList("T_BASE_UNIT." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_UNIT.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_UNIT model)
        {
            return DalT_BASE_UNIT.InsertData(model);
        }

        public static void UpdateData(T_BASE_UNIT model)
        {
            DalT_BASE_UNIT.UpdateData(model);
        }

    } 
}

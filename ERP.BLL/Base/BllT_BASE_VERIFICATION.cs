﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_VERIFICATION 
   {
        public static IList<T_BASE_VERIFICATION> GetIList(string SelectName,T_BASE_VERIFICATION model)
        {
            return DalT_BASE_VERIFICATION.GetIList("T_BASE_VERIFICATION." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_VERIFICATION.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_VERIFICATION model)
        {
            return DalT_BASE_VERIFICATION.InsertData(model);
        }

        public static void UpdateData(T_BASE_VERIFICATION model)
        {
            DalT_BASE_VERIFICATION.UpdateData(model);
        }

    } 
}

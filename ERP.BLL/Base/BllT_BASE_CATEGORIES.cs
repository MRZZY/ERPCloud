﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_CATEGORIES 
   {
        public static IList<T_BASE_CATEGORIES> GetIList(string SelectName,T_BASE_CATEGORIES model)
        {
            return DalT_BASE_CATEGORIES.GetIList("T_BASE_CATEGORIES." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_CATEGORIES.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_CATEGORIES model)
        {
            return DalT_BASE_CATEGORIES.InsertData(model);
        }

        public static void UpdateData(T_BASE_CATEGORIES model)
        {
            DalT_BASE_CATEGORIES.UpdateData(model);
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_SUPPLIER 
   {
        public static IList<T_BASE_SUPPLIER> GetIList(string SelectName,T_BASE_SUPPLIER model)
        {
            return DalT_BASE_SUPPLIER.GetIList("T_BASE_SUPPLIER." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_SUPPLIER.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_SUPPLIER model)
        {
            return DalT_BASE_SUPPLIER.InsertData(model);
        }

        public static void UpdateData(T_BASE_SUPPLIER model)
        {
            DalT_BASE_SUPPLIER.UpdateData(model);
        }

    } 
}

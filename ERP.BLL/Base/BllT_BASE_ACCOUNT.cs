﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_ACCOUNT 
   {
        public static IList<T_BASE_ACCOUNT> GetIList(string SelectName,T_BASE_ACCOUNT model)
        {
            return DalT_BASE_ACCOUNT.GetIList("T_BASE_ACCOUNT." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_ACCOUNT.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_ACCOUNT model)
        {
            return DalT_BASE_ACCOUNT.InsertData(model);
        }

        public static void UpdateData(T_BASE_ACCOUNT model)
        {
            DalT_BASE_ACCOUNT.UpdateData(model);
        }

    } 
}

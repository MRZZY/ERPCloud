﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_USER 
   {
        public static IList<T_BASE_USER> GetIList(string SelectName,T_BASE_USER model)
        {
            return DalT_BASE_USER.GetIList("T_BASE_USER." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_USER.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_USER model)
        {
            return DalT_BASE_USER.InsertData(model);
        }

        public static void UpdateData(T_BASE_USER model)
        {
            DalT_BASE_USER.UpdateData(model);
        }

    } 
}

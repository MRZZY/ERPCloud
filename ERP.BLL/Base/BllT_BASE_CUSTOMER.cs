﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_CUSTOMER 
   {
        public static IList<T_BASE_CUSTOMER> GetIList(string SelectName,T_BASE_CUSTOMER model)
        {
            return DalT_BASE_CUSTOMER.GetIList("T_BASE_CUSTOMER." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_CUSTOMER.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_CUSTOMER model)
        {
            return DalT_BASE_CUSTOMER.InsertData(model);
        }

        public static void UpdateData(T_BASE_CUSTOMER model)
        {
            DalT_BASE_CUSTOMER.UpdateData(model);
        }

    } 
}

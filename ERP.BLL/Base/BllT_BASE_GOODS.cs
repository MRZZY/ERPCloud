﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_GOODS 
   {
        public static IList<T_BASE_GOODS> GetIList(string SelectName,T_BASE_GOODS model)
        {
            return DalT_BASE_GOODS.GetIList("T_BASE_GOODS." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_GOODS.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_GOODS model)
        {
            return DalT_BASE_GOODS.InsertData(model);
        }

        public static void UpdateData(T_BASE_GOODS model)
        {
            DalT_BASE_GOODS.UpdateData(model);
        }

    } 
}

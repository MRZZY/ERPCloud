﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_ENTERPRISE 
   {
        public static IList<T_BASE_ENTERPRISE> GetIList(string SelectName,T_BASE_ENTERPRISE model)
        {
            return DalT_BASE_ENTERPRISE.GetIList("T_BASE_ENTERPRISE." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_ENTERPRISE.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_ENTERPRISE model)
        {
            return DalT_BASE_ENTERPRISE.InsertData(model);
        }

        public static void UpdateData(T_BASE_ENTERPRISE model)
        {
            DalT_BASE_ENTERPRISE.UpdateData(model);
        }

    } 
}

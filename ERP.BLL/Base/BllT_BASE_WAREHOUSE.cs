﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.Base;
using ERP.DAL.Base; 

namespace ERP.BLL.Base
{
    public class BllT_BASE_WAREHOUSE 
   {
        public static IList<T_BASE_WAREHOUSE> GetIList(string SelectName,T_BASE_WAREHOUSE model)
        {
            return DalT_BASE_WAREHOUSE.GetIList("T_BASE_WAREHOUSE." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_BASE_WAREHOUSE.DeleteData(id);
        }

        public static decimal InsertData(T_BASE_WAREHOUSE model)
        {
            return DalT_BASE_WAREHOUSE.InsertData(model);
        }

        public static void UpdateData(T_BASE_WAREHOUSE model)
        {
            DalT_BASE_WAREHOUSE.UpdateData(model);
        }

    } 
}

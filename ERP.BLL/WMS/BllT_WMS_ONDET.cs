﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;
using ERP.DAL.WMS; 

namespace ERP.BLL.WMS
{
    public class BllT_WMS_ONDET 
   {
        public static IList<T_WMS_ONDET> GetIList(string SelectName,T_WMS_ONDET model)
        {
            return DalT_WMS_ONDET.GetIList("T_WMS_ONDET." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_WMS_ONDET.DeleteData(id);
        }

        public static void DeleteDataAll(decimal id)
        {
            DalT_WMS_ONDET.DeleteDataAll(id);
        }

        public static void InsertData(List<T_WMS_ONDET> list)
        {
            DalT_WMS_ONDET.InsertData(list);
        }

        public static decimal InsertData(T_WMS_ONDET model)
        {
            return DalT_WMS_ONDET.InsertData(model);
        }

        public static void UpdateData(T_WMS_ONDET model)
        {
            DalT_WMS_ONDET.UpdateData(model);
        }

    } 
}

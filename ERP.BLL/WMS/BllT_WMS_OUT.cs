﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;
using ERP.DAL.WMS; 

namespace ERP.BLL.WMS
{
    public class BllT_WMS_OUT 
   {
        public static IList<T_WMS_OUT> GetIList(string SelectName,T_WMS_OUT model)
        {
            return DalT_WMS_OUT.GetIList("T_WMS_OUT." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_WMS_OUT.DeleteData(id);
        }

        public static decimal InsertData(T_WMS_OUT model)
        {
            return DalT_WMS_OUT.InsertData(model);
        }

        public static void UpdateData(T_WMS_OUT model)
        {
            DalT_WMS_OUT.UpdateData(model);
        }

    } 
}

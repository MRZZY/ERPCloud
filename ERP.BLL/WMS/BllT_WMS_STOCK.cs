﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;
using ERP.DAL.WMS; 

namespace ERP.BLL.WMS
{
    public class BllT_WMS_STOCK 
   {
        public static IList<T_WMS_STOCK> GetIList(string SelectName,T_WMS_STOCK model)
        {
            return DalT_WMS_STOCK.GetIList("T_WMS_STOCK." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_WMS_STOCK.DeleteData(id);
        }

        public static decimal InsertData(T_WMS_STOCK model)
        {
            return DalT_WMS_STOCK.InsertData(model);
        }

        public static void InsertData(List<T_WMS_STOCK> list)
        {
            DalT_WMS_STOCK.InsertData(list);
        }

        public static void UpStockData(List<T_WMS_STOCK> list)
        {
            DalT_WMS_STOCK.UpStockData(list);
        }

        public static void UpdateData(T_WMS_STOCK model)
        {
            DalT_WMS_STOCK.UpdateData(model);
        }

    } 
}

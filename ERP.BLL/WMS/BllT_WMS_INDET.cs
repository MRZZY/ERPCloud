﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;
using ERP.DAL.WMS; 

namespace ERP.BLL.WMS
{
    public class BllT_WMS_INDET 
   {
        public static IList<T_WMS_INDET> GetIList(string SelectName,T_WMS_INDET model)
        {
            return DalT_WMS_INDET.GetIList("T_WMS_INDET." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_WMS_INDET.DeleteData(id);
        }

        public static void DeleteDataAll(decimal id)
        {
            DalT_WMS_INDET.DeleteDataAll(id);
        }

        public static decimal InsertData(T_WMS_INDET model)
        {
            return DalT_WMS_INDET.InsertData(model);
        }

        public static void InsertData(List<T_WMS_INDET> list)
        {
            DalT_WMS_INDET.InsertData(list);
        }

        public static void UpdateData(T_WMS_INDET model)
        {
            DalT_WMS_INDET.UpdateData(model);
        }

    } 
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ERP.Model.WMS;
using ERP.DAL.WMS; 

namespace ERP.BLL.WMS
{
    public class BllT_WMS_IN 
   {
        public static IList<T_WMS_IN> GetIList(string SelectName,T_WMS_IN model)
        {
            return DalT_WMS_IN.GetIList("T_WMS_IN." + SelectName, model);
        }

        public static void DeleteData(int id)
        {
            DalT_WMS_IN.DeleteData(id);
        }

        public static decimal InsertData(T_WMS_IN model)
        {
            return DalT_WMS_IN.InsertData(model);
        }

        public static void UpdateData(T_WMS_IN model)
        {
            DalT_WMS_IN.UpdateData(model);
        }

    } 
}
